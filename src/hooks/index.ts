import { ActivityMode } from "@capytale/activity.js/activity/activitySession";
import { useAppSelector } from "../app/hooks";
import {
  selectMode,
  selectWorkflow,
} from "../features/activityData/activityDataSlice";

export const useMode = () => {
  const mode: ActivityMode = useAppSelector(selectMode);
  return mode;
};

export const useWorkflow = () => {
  const workflow = useAppSelector(selectWorkflow);
  return workflow;
};
