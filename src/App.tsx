import { Splitter, SplitterPanel } from "primereact/splitter";
import Navbar from "./features/navbar";

import styles from "./app.module.scss";
import Pedago from "./features/pedago";
import { useAppDispatch, useAppSelector } from "./app/hooks";
import { selectThemeIsDark } from "./features/theming/themingSlice";

import { Ripple } from "primereact/ripple";
import { classNames } from "primereact/utils";
import {
  selectIsPedagoVisible,
  selectOrientation,
  selectShowSaveForStudents,
  toggleIsPedagoVisible,
} from "./features/layout/layoutSlice";
import { FC, KeyboardEvent, PropsWithChildren, useCallback } from "react";
import { Tooltip } from "primereact/tooltip";
import {
  selectHasGradingOrComments,
  selectHasInstructions,
  selectIsDirty,
  selectMode,
  selectShowSaveButton,
} from "./features/activityData/activityDataSlice";
import settings from "./settings";
import ReviewNavbar from "./features/navbar/review-navbar";
import { useSave } from "./features/activityData/hooks";
import PreviewDialog from "./features/functionalities/PreviewDialog";

type AppProps = PropsWithChildren<{}>;

const App: FC<AppProps> = (props) => {
  const isDark = useAppSelector(selectThemeIsDark) as boolean;
  const mode = useAppSelector(selectMode);
  const isHorizontal = useAppSelector(selectOrientation) === "horizontal";
  const isPedagoVisible = useAppSelector(selectIsPedagoVisible) as boolean;
  const hasInstructions = useAppSelector(selectHasInstructions);
  const hasGradingOrComments = useAppSelector(selectHasGradingOrComments);
  const hasPedago =
    hasInstructions || hasGradingOrComments || mode === "review";
  const dispatch = useAppDispatch();
  const showPedago = hasPedago && isPedagoVisible;
  const isDirty = useAppSelector(selectIsDirty);
  const save = useSave();

  const showSaveButton = useAppSelector(selectShowSaveButton);
  const showSaveForStudents = useAppSelector(selectShowSaveForStudents);
  const hasSaveButton =
    showSaveButton && !(mode === "assignment" && !showSaveForStudents);

  const handleCtrlS = useCallback(
    (e: KeyboardEvent<HTMLDivElement>) => {
      if ((e.ctrlKey || e.metaKey) && e.key === "s") {
        e.preventDefault();
        if (hasSaveButton && isDirty) save(); // Checks if can save inside of save()
      }
    },
    [hasSaveButton, isDirty, save],
  );

  const pedagoOpenLabel = hasPedago
    ? "Afficher les consignes"
    : mode === "create"
      ? "Ce type d'activité n'accepte pas de consigne"
      : "Pas de consignes ni de note";

  return (
    <div
      className={classNames(
        styles.app,
        isDark ? "dark-theme" : "light-theme",
        isHorizontal ? "layout-horizontal" : "layout-vertical",
      )}
      onKeyDown={handleCtrlS}
    >
      <div className={styles.navbarContainer}>
        <Navbar />
        {mode === "review" && <ReviewNavbar />}
      </div>
      <div className={styles.pedagoContainer} data-show-pedago={showPedago}>
        <div
          className={classNames(
            styles.hiddenPedago,
            hasPedago ? null : styles.noPedago,
          )}
        >
          <div
            className={classNames(
              styles.hiddenPedagoButton,
              hasPedago ? "p-ripple" : null,
            )}
            onClick={
              hasPedago ? () => dispatch(toggleIsPedagoVisible()) : undefined
            }
            data-pr-tooltip={pedagoOpenLabel}
            aria-label={pedagoOpenLabel}
            role={hasPedago ? "button" : "note"}
          >
            <i
              className={classNames(
                "pi",
                hasPedago
                  ? isHorizontal
                    ? "pi-angle-double-down"
                    : "pi-angle-double-right"
                  : "pi-minus-circle",
              )}
            />
            <Ripple />
          </div>
          <Tooltip
            target={"." + styles.hiddenPedagoButton}
            showDelay={settings.TOOLTIP_SHOW_DELAY}
            position={isHorizontal ? "bottom" : "right"}
            mouseTrack={!isHorizontal}
          />
        </div>
        <Splitter
          className={styles.appPedagoSplitter}
          layout={isHorizontal ? "vertical" : "horizontal"}
        >
          <SplitterPanel minSize={15} size={30} className={styles.pedagoPanel}>
            <Pedago key="pedago" />
          </SplitterPanel>
          <SplitterPanel minSize={40} size={70} className={styles.contentPanel}>
            <div className="meta-player-content-cover"></div>
            <div id="meta-player-content" aria-label="Activité">
              {props.children}
            </div>
          </SplitterPanel>
        </Splitter>
      </div>
      <PreviewDialog />
    </div>
  );
};

export default App;
