import React, { FC, PropsWithChildren, useMemo } from "react";

import { Provider } from "react-redux";

import { APIOptions, PrimeReactProvider } from "primereact/api";
import "primeicons/primeicons.css";

import App from "./App";
import { makeStore } from "./app/store";
import "./index.css";
import ThemeSwitcher from "./features/theming/ThemeSwitcher";
import {
  ActivityJSProvider,
  useActivityJS,
} from "./features/activityJS/ActivityJSProvider";
import { LoadOptions } from "./features/activityJS/internal-hooks";
import { ErrorBoundary } from "./utils/ErrorBoundary";
import Saver from "./features/activityJS/Saver";
import {
  CapytaleAntiCheat,
  ContentHidingMethod,
  ExitDetectionMethod,
} from "@capytale/capytale-anti-triche";
import { useAppSelector } from "./app/hooks";
import {
  selectActivityInfo,
  selectAntiCheat,
  selectHasAntiCheat,
  selectIsAntiCheatExitDetectionDisabled,
  selectIsDirty,
  selectReturnUrl,
} from "./features/activityData/activityDataSlice";

import { initialState as layoutInitialState } from "./features/layout/layoutSlice";
import ExitWarning from "./features/activityData/ExitWarning";
import type { AntiCheatOptions, UIOptions } from "./types";

type MetaPlayerProps = PropsWithChildren<{
  activityJSOptions?: LoadOptions;
  antiCheatOptions?: Partial<AntiCheatOptions>;
  uiOptions?: Partial<UIOptions>;
}>;

const MetaPlayer: FC<MetaPlayerProps> = (props) => {
  const primeSettings: Partial<APIOptions> = {
    ripple: true,
  };
  const antiCheatOptions: AntiCheatOptions = {
    preserveDom: true,
    hasIframes: false,
    ...props.antiCheatOptions,
  };
  const uiOptions: UIOptions = {
    closePedagoByDefault: false,
    noWorkflow: false,
    noSaveForStudents: false,
    ...props.uiOptions,
  };
  const store = useMemo(
    () =>
      makeStore({
        layout: {
          ...layoutInitialState,
          isPedagoVisible: !uiOptions.closePedagoByDefault,
          showWorkflow: !uiOptions.noWorkflow,
          showSaveForStudents: !uiOptions.noSaveForStudents,
        },
      }),
    [],
  );
  return (
    <PrimeReactProvider value={primeSettings}>
      <Provider store={store}>
        <ThemeSwitcher />
        <ExitWarning />
        <ErrorBoundary fallback={<div>Une erreur est survenue</div>}>
          <ActivityJSProvider
            options={props.activityJSOptions}
            loadingFallback={<div>Chargement de l'activité...</div>}
            errorFallback={({ children }) => (
              <div style={{ marginLeft: "1rem", marginRight: "1rem" }}>
                <p>Erreur lors du chargement de l'activité : {children}</p>
                <p>
                  Êtes-vous bien connecté(e) à Capytale ? Avez-vous bien le
                  droit d'accéder à cette ressource ?
                </p>
                <p>
                  <a href="/">Retour à l'accueil</a>
                </p>
              </div>
            )}
          >
            <Saver />
            <MetaPlayerContent antiCheatOptions={antiCheatOptions}>
              <App>{props.children}</App>
            </MetaPlayerContent>
          </ActivityJSProvider>
        </ErrorBoundary>
      </Provider>
    </PrimeReactProvider>
  );
};

type MetaPlayerContentProps = {
  antiCheatOptions: AntiCheatOptions;
};

const MetaPlayerContent: React.FC<PropsWithChildren<MetaPlayerContentProps>> = (
  props,
) => {
  const isDirty = useAppSelector(selectIsDirty);
  const antiCheat = useAppSelector(selectAntiCheat);
  const hasAntiCheat = useAppSelector(selectHasAntiCheat);
  const activityInfo = useAppSelector(selectActivityInfo);
  const returnUrl = useAppSelector(selectReturnUrl);
  const isAntiCheatExitDetectionDisabled = useAppSelector(
    selectIsAntiCheatExitDetectionDisabled,
  );
  const studentName = activityInfo.studentInfo
    ? `${activityInfo.studentInfo.firstName} ${activityInfo.studentInfo.lastName}`
    : "";
  const activityJS = useActivityJS();

  const touch = () => {
    if (
      activityJS.activitySession == null ||
      activityJS.activitySession.activityBunch.assignmentNode == null
    ) {
      console.error("ActivityJS data not loaded or touch not available");
      return Promise.resolve();
    } else {
      return activityJS.activitySession.activityBunch.assignmentNode.touch();
    }
  };

  return (
    <CapytaleAntiCheat
      enabled={hasAntiCheat}
      hashedPassword={antiCheat?.passwordHash || null}
      startLocked={antiCheat?.startLocked || null}
      activityTitle={activityInfo.title}
      studentName={studentName}
      isDirty={isDirty}
      returnUrl={returnUrl}
      dbTouchFunction={touch}
      disableExitDetection={isAntiCheatExitDetectionDisabled}
      contentClassName="anti-cheat-content"
      exitDetectionMethod={
        props.antiCheatOptions.hasIframes
          ? ExitDetectionMethod.VISIBILITY_CHANGE
          : ExitDetectionMethod.BLUR
      }
      contentHidingMethod={
        props.antiCheatOptions.preserveDom
          ? ContentHidingMethod.DISPLAY_NONE
          : ContentHidingMethod.REMOVE_FROM_DOM
      }
    >
      {props.children}
    </CapytaleAntiCheat>
  );
};

export default MetaPlayer;
