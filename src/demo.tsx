import React, { FC } from "react";
import { createRoot } from "react-dom/client";

import { MetaPlayer } from ".";
import { ActivityQuickActionsSetter } from "./features/navbar/activity-menu/ActivityQuickActions";
import { ActivitySidebarActionsSetter } from "./features/navbar/sidebars/ActivitySidebarActions";
// import { useActivityJS } from "./features/activityJS/ActivityJSProvider";
import BeforeSaveAction from "./features/activityJS/BeforeSaveAction";
import MetaPlayerOptionsSetter from "./features/activityData/MetaPlayerOptionsSetter";

const DemoActivity: FC = () => {
  // const activityJs = useActivityJS();
  return (
    <>
      <ActivityQuickActionsSetter
        actions={[
          {
            type: "action",
            title: "Pause",
            icon: "pi pi-pause",
            action: () => console.log("Pause"),
          },
          {
            type: "action",
            title: "Play",
            icon: "pi pi-play",
            action: () => console.log("Play"),
          },
        ]}
      />
      <ActivitySidebarActionsSetter
        actions={[
          {
            type: "action",
            title: "Pause",
            icon: "pi pi-pause",
            action: () => console.log("Pause"),
          },
          {
            type: "action",
            title: "Play",
            icon: "pi pi-play",
            action: () => console.log("Play"),
          },
        ]}
      />
      <BeforeSaveAction
        name="demo"
        callback={() => console.log("Before save")}
      />
      <BeforeSaveAction
        name="demo2"
        callback={() => {
          console.log("Before save 2");
          // throw new Error("Error in before save 2");
        }}
      />
      Activité
    </>
  );
};

const container = document.getElementById("root");

if (container) {
  const root = createRoot(container);

  root.render(
    <React.StrictMode>
      <MetaPlayer>
        <MetaPlayerOptionsSetter
          options={{
            hasInstructions: true,
            pedagoLayout: "default-horizontal",
            supportsLightTheme: true,
            supportsDarkTheme: true,
          }}
        />
        <DemoActivity />
      </MetaPlayer>
    </React.StrictMode>,
  );
} else {
  throw new Error(
    "Root element with ID 'root' was not found in the document. Ensure there is a corresponding HTML element with the ID 'root' in your HTML file.",
  );
}
