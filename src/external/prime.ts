import { Toast } from "primereact/toast";
import type { ToastMessage } from "primereact/toast";

export { Toast };
export type { ToastMessage };
