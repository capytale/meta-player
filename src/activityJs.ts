import evaluationApi from "@capytale/activity.js/backend/capytale/evaluation";
import type { Evaluation } from "@capytale/activity.js/activity/evaluation/evaluation";

export { evaluationApi };
export type { Evaluation };
