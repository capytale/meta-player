import "primeicons/primeicons.css";
import "./index.css";

import MetaPlayer from "./MetaPlayer";
import MetaPlayerOptionsSetter from "./features/activityData/MetaPlayerOptionsSetter";
import BeforeSaveAction from "./features/activityJS/BeforeSaveAction";
import BeforeResetAction from "./features/activityJS/BeforeResetAction";
import AfterSaveAction from "./features/activityJS/AfterSaveAction";
import AfterResetAction from "./features/activityJS/AfterResetAction";
import { useMode, useWorkflow } from "./hooks";
import { useThemeType } from "./features/theming/hooks";
import {
  useActivityJS,
  useActivityJsEssentials,
} from "./features/activityJS/ActivityJSProvider";
import {
  useNotifyIsDirty,
  useCanSave,
  useSave,
} from "./features/activityData/hooks";
import { useOrientation } from "./features/layout/hooks";
import { ActivitySidebarActionsSetter } from "./features/navbar/sidebars/ActivitySidebarActions";
import { ActivityQuickActionsSetter } from "./features/navbar/activity-menu/ActivityQuickActions";
import ActivitySettingsSetter from "./features/activitySettings/ActivitySettingsSetter";
import IsDirtySetter from "./features/activityData/IsDirtySetter";
import AttachedFilesFunctionality from "./features/functionalities/AttachedFilesFunctionality";
import { useRefreshAttachedFiles } from "./features/functionalities/hooks";
import { useActivitySettings } from "./features/activitySettings/hooks";
import {
  usePreviewTextFile,
  usePreviewImageFile,
} from "./features/functionalities/hooks";
import { Toast } from "./external/prime";
import type { ToastMessage } from "./external/prime";
import type {
  AttachedFileData,
  UploadedFileInfo,
} from "./features/functionalities/functionalitiesSlice";

export {
  MetaPlayer,
  MetaPlayerOptionsSetter,
  BeforeSaveAction,
  BeforeResetAction,
  AfterSaveAction,
  AfterResetAction,
  useMode,
  useWorkflow,
  useActivityJS,
  useActivityJsEssentials,
  useNotifyIsDirty,
  useCanSave,
  useSave,
  useOrientation,
  useThemeType,
  useActivitySettings,
  usePreviewTextFile,
  usePreviewImageFile,
  ActivitySidebarActionsSetter,
  ActivityQuickActionsSetter,
  ActivitySettingsSetter,
  IsDirtySetter,
  AttachedFilesFunctionality,
  useRefreshAttachedFiles,
  Toast,
};
export type { AttachedFileData, UploadedFileInfo, ToastMessage };
