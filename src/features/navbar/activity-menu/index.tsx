import { memo, useMemo, useState } from "react";
import styles from "./style.module.scss";

import { Button } from "primereact/button";
import { Sidebar } from "primereact/sidebar";
import SettingsSidebarContent from "../sidebars/SettingsSidebarContent";
import settings from "../../../settings";
import ActivityQuickActions from "./ActivityQuickActions";
import useFullscreen from "../../../utils/useFullscreen";
import { useActivityJS } from "../../activityJS/ActivityJSProvider";
import { Dialog } from "primereact/dialog";
import capytaleUI from "@capytale/activity.js/backend/capytale/ui";
import { useAppSelector } from "../../../app/hooks";
import { selectHasAntiCheat } from "../../activityData/activityDataSlice";
import AttachedFilesSidebarContent from "../sidebars/AttachedFilesSidebarContent";
import { Badge } from "primereact/badge";
import { useAttachedFiles } from "../../functionalities/hooks";
import {
  selectAttachedFilesEnabled,
  selectAttachedFilesOptions,
} from "../../functionalities/functionalitiesSlice";

const ActivityMenu: React.FC = memo(() => {
  const [settingsSidebarVisible, setSettingsSidebarVisible] = useState(false);
  const [attachedFilesSidebarVisible, setAttachedFilesSidebarVisible] =
    useState(false);
  const [wantFullscreen, setWantFullscreen] = useState(false);
  const isFullscreen = useFullscreen(wantFullscreen, () =>
    setWantFullscreen(false),
  );
  const isInIframe = useMemo(() => capytaleUI.isInCapytaletIframe(), []);
  const [helpDialogVisible, setHelpDialogVisible] = useState(false);
  const activityJS = useActivityJS();
  const hasAntiCheat = useAppSelector(selectHasAntiCheat);
  const attachedFiles = useAttachedFiles();
  const attachedFilesEnabled = useAppSelector(selectAttachedFilesEnabled);
  const attachedFilesOptions = useAppSelector(selectAttachedFilesOptions);
  return (
    <div className={styles.activityMenu}>
      <ActivityQuickActions />
      {attachedFilesEnabled && (
        <Button
          aria-label="Afficher les fichiers joints"
          severity="secondary"
          size="small"
          icon={
            <i className="pi pi-paperclip p-overlay-badge">
              {attachedFiles.length > 0 && (
                <Badge value={attachedFiles.length}></Badge>
              )}
            </i>
          }
          outlined
          tooltip="Fichiers joints"
          tooltipOptions={{
            position: "left",
            showDelay: settings.TOOLTIP_SHOW_DELAY,
          }}
          className="p-overlay-badge"
          onClick={() => {
            attachedFilesOptions.onViewFiles?.();
            setAttachedFilesSidebarVisible(true);
          }}
        />
      )}
      <Button
        aria-label="Afficher les paramètres"
        severity="secondary"
        size="small"
        icon="pi pi-cog" // pi-ellipsis-v
        outlined
        tooltip="Paramètres"
        tooltipOptions={{
          position: "left",
          showDelay: settings.TOOLTIP_SHOW_DELAY,
        }}
        onClick={() => setSettingsSidebarVisible(true)}
      />
      {!hasAntiCheat && (
        <Button
          aria-label="Mettre en plein écran"
          severity="secondary"
          size="small"
          icon={isFullscreen ? "pi pi-expand" : "pi pi-expand"}
          text
          tooltip="Plein écran"
          tooltipOptions={{
            position: "bottom",
            showDelay: settings.TOOLTIP_SHOW_DELAY,
          }}
          onClick={() => setWantFullscreen((prev) => !prev)}
        />
      )}
      {isInIframe && (
        <Button
          aria-label="Retour à la liste des activités (droite)"
          severity="secondary"
          size="small"
          icon={"pi pi-times"}
          text
          tooltip="Retour à la liste des activités"
          tooltipOptions={{
            position: "left",
            showDelay: settings.TOOLTIP_SHOW_DELAY,
          }}
          onClick={() => capytaleUI.closeCapytaleIframe()}
        />
      )}
      <Sidebar
        header="Paramètres"
        visible={settingsSidebarVisible}
        position="right"
        onHide={() => setSettingsSidebarVisible(false)}
      >
        <SettingsSidebarContent
          showHelp={
            activityJS.activitySession?.type.helpUrl
              ? () => {
                  setHelpDialogVisible(true);
                  setSettingsSidebarVisible(false);
                }
              : undefined
          }
        />
      </Sidebar>
      <Sidebar
        header="Fichiers joints"
        visible={attachedFilesSidebarVisible}
        position="right"
        onHide={() => setAttachedFilesSidebarVisible(false)}
      >
        <AttachedFilesSidebarContent />
      </Sidebar>
      {activityJS.activitySession?.type.helpUrl && (
        <Dialog
          id="metaPlayerHelpDialog"
          header="Documentation"
          visible={helpDialogVisible}
          onHide={() => setHelpDialogVisible(false)}
          maximizable={true}
        >
          <iframe
            src={activityJS.activitySession?.type.helpUrl}
            style={{ width: "100%", height: "100%" }}
            title="Documentation"
          />
        </Dialog>
      )}
    </div>
  );
});

export default ActivityMenu;
