import { FC, useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import {
  QuickAction,
  selectQuickActions,
  setQuickActions,
  usePerformQuickAction,
} from "../navbarSlice";
import { Button } from "primereact/button";
import settings from "../../../settings";

const ActivityQuickActions: FC<{}> = () => {
  const quickActions = useAppSelector(selectQuickActions);
  const performQuickAction = usePerformQuickAction();
  return (
    <>
      {quickActions.map((action) => (
        <Button
          aria-label={action.title}
          key={action.title}
          severity="secondary"
          size="small"
          icon={action.icon}
          onClick={() => {
            performQuickAction(action);
          }}
          outlined
          tooltip={action.title}
          tooltipOptions={{
            position: "bottom",
            showDelay: settings.TOOLTIP_SHOW_DELAY,
          }}
        />
      ))}
    </>
  );
};

type ActivityQuickActionsSetterProps = {
  actions: QuickAction[];
};

export const ActivityQuickActionsSetter: FC<ActivityQuickActionsSetterProps> = (
  props,
) => {
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(setQuickActions(props.actions));
    return () => {
      dispatch(setQuickActions([]));
    };
  }, [props.actions]);

  return <></>;
};

export default ActivityQuickActions;
