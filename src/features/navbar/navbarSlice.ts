import type { PayloadAction } from "@reduxjs/toolkit";
import { createAppSlice } from "../../app/createAppSlice";
import { Callback, useFileUpload } from "use-file-upload";
export type QuickAction =
  | {
      type: "action";
      title: string;
      icon: string;
      action: () => void;
    }
  | {
      type: "file-upload";
      accept?: string;
      title: string;
      icon: string;
      action: (file: {
        source: URL;
        name: string;
        size: number;
        file: File;
      }) => void;
    };

export function usePerformQuickAction() {
  const [_, selectFile] = useFileUpload();
  return (action: QuickAction) => {
    if (action.type === "action") {
      action.action();
    } else if (action.type === "file-upload") {
      selectFile(
        { accept: action.accept, multiple: false },
        action.action as Callback,
      );
    }
  };
}

export interface NavbarState {
  quickActions: QuickAction[];
  sidebarActions: QuickAction[];
}

const initialState: NavbarState = {
  quickActions: [],
  sidebarActions: [],
};

// If you are not using async thunks you can use the standalone `createSlice`.
export const navbarSlice = createAppSlice({
  name: "navbar",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: (create) => ({
    setQuickActions: create.reducer(
      (state, action: PayloadAction<QuickAction[]>) => {
        state.quickActions = action.payload;
      },
    ),
    setSidebarActions: create.reducer(
      (state, action: PayloadAction<QuickAction[]>) => {
        state.sidebarActions = action.payload;
      },
    ),
  }),
  // You can define your selectors here. These selectors receive the slice
  // state as their first argument.
  selectors: {
    selectQuickActions: (navbar) => navbar.quickActions,
    selectSidebarActions: (navbar) => navbar.sidebarActions,
  },
});

// Action creators are generated for each case reducer function.
export const { setQuickActions, setSidebarActions } = navbarSlice.actions;

// Selectors returned by `slice.selectors` take the root state as their first argument.
export const { selectQuickActions, selectSidebarActions } =
  navbarSlice.selectors;
