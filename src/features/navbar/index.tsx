import { MD } from "../../utils/breakpoints";
import { useWindowSize } from "@uidotdev/usehooks";
import ActivityInfo from "./activity-info";
import ActivityMenu from "./activity-menu";
import CapytaleMenu from "./capytale-menu";
import styles from "./style.module.scss";
import { useMemo } from "react";
import { useAppSelector } from "../../app/hooks";
import { selectReturnUrl } from "../activityData/activityDataSlice";
import capytaleUI from "@capytale/activity.js/backend/capytale/ui";

const Navbar: React.FC = () => {
  const returnUrl = useAppSelector(selectReturnUrl);
  const windowsSize = useWindowSize();
  const isSmall = useMemo(
    () => windowsSize.width && windowsSize.width < MD,
    [windowsSize.width],
  );
  const isInIframe = useMemo(() => capytaleUI.isInCapytaletIframe(), []);
  const logoText = isSmall ? "C" : "CAPYTALE";
  return (
    <div className={styles.navbar}>
      <div className={styles.navbarContainer}>
        <div className={styles.navbarLogo}>
          {!isInIframe && (
            <a href={returnUrl} aria-label="Capytale (accueil)">
              {logoText}
            </a>
          )}
        </div>
        <CapytaleMenu />
        <ActivityInfo />
        <ActivityMenu />
      </div>
    </div>
  );
};

export default Navbar;
