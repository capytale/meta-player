import { FC, useMemo, useState } from "react";
import Countdown from "./Countdown";
import { Button } from "primereact/button";
import { Dialog } from "primereact/dialog";
import { ML } from "../../../utils/breakpoints";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import {
  selectHasEvaluations,
  selectIsDirty,
  selectMode,
  selectPreventEditIfHasEvaluations,
  selectSaveState,
  selectShowSaveButton,
  setSaveState,
} from "../../activityData/activityDataSlice";
import { useWindowSize } from "@uidotdev/usehooks";
import { selectShowSaveForStudents } from "../../layout/layoutSlice";
import settings from "../../../settings";

const CountdownAndSaveButton: FC = () => {
  const dispatch = useAppDispatch();
  const saveState = useAppSelector(selectSaveState);
  const windowsSize = useWindowSize();
  const isQuiteSmall = useMemo(
    () => windowsSize.width && windowsSize.width < ML,
    [windowsSize.width],
  );
  const isDirty = useAppSelector(selectIsDirty);
  const preventEditIfHasEvaluations = useAppSelector(
    selectPreventEditIfHasEvaluations,
  );
  const hasEvaluations = useAppSelector(selectHasEvaluations);

  const mode = useAppSelector(selectMode);
  const showSaveButton = useAppSelector(selectShowSaveButton);
  const showSaveForStudents = useAppSelector(selectShowSaveForStudents);
  const hasSaveButton =
    showSaveButton && !(mode === "assignment" && !showSaveForStudents);

  const [editForbiddenDialogVisible, setEditForbiddenDialogVisible] =
    useState(false);

  if (!hasSaveButton) {
    return null;
  }

  if (mode === "create" && hasEvaluations && preventEditIfHasEvaluations) {
    return (
      <>
        <Button
          label={isQuiteSmall ? undefined : "Modification interdite"}
          aria-label="Modification interdite"
          severity="danger"
          size="small"
          icon="pi pi-save"
          loading={saveState !== "idle"}
          onClick={() => {
            setEditForbiddenDialogVisible(true);
          }}
          tooltip={"Évaluations déjà attribuées"}
          tooltipOptions={{
            position: "bottom",
            showDelay: settings.TOOLTIP_SHOW_DELAY,
            showOnDisabled: true,
          }}
        />
        <Dialog
          header="Modification interdite"
          visible={editForbiddenDialogVisible}
          onHide={() => {
            if (!editForbiddenDialogVisible) return;
            setEditForbiddenDialogVisible(false);
          }}
          style={{ maxWidth: "35rem" }}
        >
          <p className="m-0">
            Vous avez déjà donné des évaluations à vos élèves. Ce type
            d'activité ne supporte pas la modification dans ce cas car cela
            rendrait les résultats des élèves incohérents.
          </p>
          <p className="m-0 mt-1">
            Si vous souhaitez modifier cette activité, vous pouvez la clôner et
            modifier la copie, que vous pouvez ensuite proposer à vos élèves.
          </p>
        </Dialog>
      </>
    );
  }

  return (
    <>
      <Countdown />
      <Button
        label={isQuiteSmall ? undefined : "Enregistrer"}
        aria-label="Enregistrer"
        disabled={!isDirty}
        severity={"warning"}
        size="small"
        icon="pi pi-save"
        loading={saveState !== "idle"}
        onClick={() => {
          dispatch(setSaveState("should-save"));
        }} /**
        tooltip={isDirty ? "Enregistrer l'activité" : "Rien à enregistrer"}
        tooltipOptions={{
          position: "bottom",
          showDelay: settings.TOOLTIP_SHOW_DELAY,
          showOnDisabled: true,
        }} */
      />
    </>
  );
};

export default CountdownAndSaveButton;
