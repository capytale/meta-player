import { Button } from "primereact/button";
import { ButtonGroup } from "primereact/buttongroup";
import { ConfirmDialog, confirmDialog } from "primereact/confirmdialog";
import { Toast } from "primereact/toast";
import { OverlayPanel } from "primereact/overlaypanel";

import styles from "./style.module.scss";
import { useMemo, useRef } from "react";
import { useWindowSize } from "@uidotdev/usehooks";
import { XL } from "../../../utils/breakpoints";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import {
  selectMode,
  selectSharingInfo,
  selectWorkflow,
  setSaveState,
} from "../../activityData/activityDataSlice";
import { copyToClipboard } from "../../../utils/clipboard";
import settings from "../../../settings";
import { wf } from "@capytale/activity.js/activity/field/workflow";
import capytaleUI from "@capytale/activity.js/backend/capytale/ui";
import ButtonDoubleIcon from "../../../utils/ButtonDoubleIcon";
import CardSelector from "../../../utils/CardSelector";
import { useActivityJS } from "../../activityJS/ActivityJSProvider";
import { selectShowWorkflow } from "../../layout/layoutSlice";
import CountdownAndSaveButton from "./CountdownAndSaveButton";

const CapytaleMenu: React.FC = () => {
  const dispatch = useAppDispatch();
  const activityJS = useActivityJS();
  const sharingInfo = useAppSelector(selectSharingInfo);
  const windowsSize = useWindowSize();
  const mode = useAppSelector(selectMode);
  const workflow = useAppSelector(selectWorkflow);
  const showWorkflow = useAppSelector(selectShowWorkflow);
  const isLarge = useMemo(
    () => windowsSize.width && windowsSize.width >= XL,
    [windowsSize.width],
  );
  const isInIframe = useMemo(() => capytaleUI.isInCapytaletIframe(), []);
  const toast = useRef<Toast>(null);
  const changeWorkflow = (value: wf) => {
    activityJS.activitySession!.activityBunch.assignmentNode!.workflow = value;
    dispatch(setSaveState("should-save"));
  };
  const confirmFinishAssignment = () => {
    confirmDialog({
      message:
        "Votre copie va être rendue et vous ne pourrez plus sauvegarder les modifications ultérieures.",
      header: "Attention",
      icon: "pi pi-exclamation-triangle",
      defaultFocus: "accept",
      acceptLabel: "Confirmer",
      rejectLabel: "Annuler",
      accept: () => changeWorkflow("finished"),
    });
  };
  const overlayPanelWorkflow = useRef<OverlayPanel>(null);
  return (
    <>
      <Toast ref={toast} position="bottom-right" />
      <div className={styles.capytaleMenu}>
        {isInIframe && (
          <Button
            aria-label="Retour à la liste des activités (gauche)"
            label="Retour"
            icon="pi pi-chevron-left"
            size="small"
            outlined
            onClick={() => capytaleUI.closeCapytaleIframe()}
            tooltip="Retour à la liste des activités"
            tooltipOptions={{
              position: "right",
              showDelay: settings.TOOLTIP_SHOW_DELAY,
            }}
          />
        )}
        <CountdownAndSaveButton />

        {mode === "create" && sharingInfo.code && (
          <ButtonGroup>
            <Button
              label={isLarge ? sharingInfo.code : undefined}
              aria-label="Copier le code de partage"
              severity="secondary"
              size="small"
              icon="pi pi-share-alt"
              outlined
              tooltip="Copier le code de partage"
              tooltipOptions={{
                position: "bottom",
                showDelay: settings.TOOLTIP_SHOW_DELAY,
              }}
              onClick={async () => {
                await copyToClipboard(sharingInfo.code!);
                toast.current!.show({
                  summary: "Code copié",
                  detail:
                    "Le code de partage a été copié dans le presse-papier.",
                  severity: "info",
                  life: 2000,
                });
              }}
            />
            <Button
              aria-label="Copier l'URL de partage"
              icon="pi pi-link"
              severity="secondary"
              size="small"
              outlined
              tooltip="Copier l'URL de partage"
              tooltipOptions={{
                position: "bottom",
                showDelay: settings.TOOLTIP_SHOW_DELAY,
              }}
              onClick={() => {
                copyToClipboard(sharingInfo.codeLink!).then(() => {
                  toast.current!.show({
                    summary: "URL copiée",
                    detail:
                      "L'URL de partage a été copiée dans le presse-papier.",
                    severity: "info",
                    life: 2000,
                  });
                });
              }}
            />
          </ButtonGroup>
        )}
        {showWorkflow && (
          <>
            {mode === "assignment" && workflow === "current" && (
              <Button
                aria-label="Rendre la copie"
                size="small"
                outlined
                label="Rendre"
                icon="pi pi-envelope"
                onClick={() => {
                  confirmFinishAssignment();
                }}
              />
            )}
            {mode === "assignment" && workflow !== "current" && (
              <Button
                aria-label={
                  workflow === "finished"
                    ? "Copie déjà rendue"
                    : "Copie déjà rendue et corrigée"
                }
                outlined
                label={workflow === "finished" ? "Rendue" : "Corrigée"}
                disabled
                icon={
                  workflow === "finished"
                    ? "pi pi-envelope"
                    : "pi pi-check-square"
                }
                tooltip="Copie déjà rendue"
              />
            )}
            {mode === "review" && (
              <div>
                <ButtonDoubleIcon
                  aria-label="Changer l'état de la copie"
                  severity="secondary"
                  size="small"
                  outlined
                  onClick={(e) => {
                    overlayPanelWorkflow.current!.toggle(e);
                  }}
                  label={
                    workflow === "current"
                      ? "En cours"
                      : workflow === "finished"
                        ? "Rendue"
                        : "Corrigée"
                  }
                  leftIcon={
                    "pi " +
                    (workflow === "current"
                      ? "pi pi-pencil"
                      : workflow === "finished"
                        ? "pi pi-envelope"
                        : "pi pi-check-square")
                  }
                  rightIcon="pi pi-angle-down"
                />
                <OverlayPanel
                  ref={overlayPanelWorkflow}
                  aria-label="Panneau de changement d'état de la copie"
                >
                  <h3 className={styles.overlayPanelWorkflowTitle}>
                    État de la copie
                  </h3>
                  <CardSelector
                    selected={workflow}
                    onChange={(option: wf) => {
                      changeWorkflow(option);

                      overlayPanelWorkflow.current!.hide();
                    }}
                    options={[
                      {
                        value: "current",
                        title: "En cours",
                        description:
                          "L'élève peut modifier sa copie à tout moment.",
                      },
                      {
                        value: "finished",
                        title: "Rendue",
                        description: "L'élève ne peut plus modifier sa copie.",
                      },
                      {
                        value: "corrected",
                        title: "Corrigée",
                        description:
                          "L'élève ne peut plus modifier sa copie et a reçu une correction.",
                      },
                    ]}
                  />
                </OverlayPanel>
              </div>
            )}
          </>
        )}
        <ConfirmDialog />
      </div>
    </>
  );
};

export default CapytaleMenu;
