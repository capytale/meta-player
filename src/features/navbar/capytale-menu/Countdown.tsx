import { FC, ReactNode, useCallback, useMemo, useRef, useState } from "react";
import { useInterval } from "primereact/hooks";
import { Toast } from "primereact/toast";
import { useActivityJS } from "../../activityJS/ActivityJSProvider";
import { useAppSelector } from "../../../app/hooks";
import { selectIsDirty, selectMode } from "../../activityData/activityDataSlice";
import { Button } from "primereact/button";
import { useSave } from "../../activityData/hooks";
import serverClock from "../../../utils/server-clock";

// TODO use https://capytale2.ac-paris.fr/vanilla/time-s.php
// https://forge.apps.education.fr/capytale/activity-js/-/blob/main/src/backend/capytale/clock.ts?ref_type=heads
// https://forge.apps.education.fr/capytale/activity-js/-/blob/main/src/api/time/clock.ts?ref_type=heads

const MS_PER_MINUTE = 1000 * 60;
const MS_PER_DAY = MS_PER_MINUTE * 60 * 24;
const MS_PER_YEAR = MS_PER_DAY * 365;

const Countdown: FC = () => {
  const toast = useRef<Toast>(null);
  const mode = useAppSelector(selectMode);
  const isDirty = useAppSelector(selectIsDirty);
  const oneMinuteWarningShown = useRef(false);
  const fifteenSecondsWarningShown = useRef(false);
  const shouldSaveAt30s = useRef(true);
  const save30sAttempted = useRef(false);
  const zeroSecondsWarningShown = useRef(false);
  const save = useSave();

  const showOneMinuteWarning = () => {
    oneMinuteWarningShown.current = true;
    toast.current?.show({
      severity: "warn",
      summary: "Activité non sauvegardée",
      detail: (
        <>
          <div>
            Il vous reste moins d'une minute pour rendre votre activité. Elle
            sera sauvegardée automatiquement 30 secondes avant la fin.
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              gap: "8px",
              marginTop: "8px",
              alignItems: "stretch",
            }}
          >
            <Button
              severity="danger"
              onClick={() => {
                shouldSaveAt30s.current = false;
                toast.current?.clear();
              }}
            >
              Ne pas sauvegarder (danger)
            </Button>
            <Button severity="success" onClick={save}>
              Sauvegarder maintenant
            </Button>
          </div>
        </>
      ),
      sticky: true,
      closable: false,
    });
  };

  const showFifteenSecondsWarning = () => {
    fifteenSecondsWarningShown.current = true;
    toast.current?.show({
      severity: "error",
      summary: "Activité non sauvegardée",
      detail: (
        <>
          <div>
            Il vous reste moins de 15 secondes pour rendre votre activité.
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              gap: "8px",
              marginTop: "8px",
              alignItems: "stretch",
            }}
          >
            <Button
              severity="danger"
              onClick={() => {
                save();
                toast.current?.clear();
              }}
            >
              Sauvegarder maintenant
            </Button>
          </div>
        </>
      ),
      sticky: true,
    });
  };

  const activityJS = useActivityJS();
  const deadlineMode =
    activityJS.activitySession?.activityBunch.access_tr_mode.value;
  const deadline =
    !deadlineMode || deadlineMode === "none"
      ? null
      : activityJS.activitySession?.activityBunch.access_timerange.value?.end;

  const [diffMs, setDiffMs] = useState<number | null>(null);

  const updateDiffMs = useCallback(() => {
    if (!deadline) {
      return;
    }
    const deadlineUTC = deadline.getTime();
    const nowUTC = serverClock.now();
    const newDiffMs = deadlineUTC - nowUTC;
    setDiffMs(newDiffMs);
    if (mode !== "assignment") {
      return;
    }
    if (
      newDiffMs != null &&
      newDiffMs < MS_PER_MINUTE &&
      newDiffMs > 35000 &&
      isDirty &&
      !oneMinuteWarningShown.current
    ) {
      showOneMinuteWarning();
    }
    if (
      newDiffMs != null &&
      newDiffMs < 30000 &&
      newDiffMs > 0 &&
      shouldSaveAt30s.current &&
      !save30sAttempted.current
    ) {
      if (isDirty) {
        save();
      }
      toast.current?.clear();
      save30sAttempted.current = true;
    }
    if (
      isDirty &&
      newDiffMs != null &&
      newDiffMs < 15000 &&
      newDiffMs > 0 &&
      !fifteenSecondsWarningShown.current
    ) {
      showFifteenSecondsWarning();
    }
    if (newDiffMs <= 0 && !zeroSecondsWarningShown.current) {
      zeroSecondsWarningShown.current = true;
      toast.current?.clear();
      toast.current?.show({
        severity: "info",
        summary: "Temps écoulé",
        detail: "Le temps est écoulé, il n'est plus possible de sauvegarder.",
        sticky: true,
      });
    }
  }, [deadline, mode, isDirty]);

  useInterval(updateDiffMs, 1000, !!deadline);

  const displayed = useMemo<ReactNode>(() => {
    if (!deadline || diffMs == null) {
      return null;
    }
    if (diffMs >= MS_PER_DAY) {
      return (
        <div
          aria-label="Deadline"
          style={{
            display: "flex",
            alignItems: "center",
            gap: "8px",
            marginRight: "8px",
          }}
        >
          <i className="pi pi-calendar-clock" />
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            {deadline
              .toLocaleString("fr-FR", {
                year: diffMs >= MS_PER_YEAR ? "numeric" : undefined,
                month: "short",
                day: "numeric",
                hour: "numeric",
                minute: "numeric",
              })
              .split(", ")
              .map((s, i) => (
                <div
                  key={i}
                  aria-label={i === 0 ? "Date limite" : "Heure limite"}
                >
                  {s}
                </div>
              ))}
          </div>
        </div>
      );
    }
    if (diffMs <= 0) {
      return (
        <div
          aria-label="Temps écoulé"
          style={{
            display: "flex",
            alignItems: "center",
            gap: "8px",
            marginRight: "8px",
          }}
        >
          <i className="pi pi-calendar-times" />
          <span>Temps écoulé</span>
        </div>
      );
    }
    const diffSeconds = Math.floor(diffMs / 1000);
    const seconds = diffSeconds % 60;
    const minutes = Math.floor(diffSeconds / 60) % 60;
    const hours = Math.floor(diffSeconds / 3600) % 24;
    return (
      <div
        aria-label="Temps restant"
        style={{
          display: "flex",
          alignItems: "center",
          gap: "8px",
          marginRight: "8px",
        }}
      >
        <i className="pi pi-hourglass" />
        {`${hours.toString().padStart(2, "0")}:${minutes
          .toString()
          .padStart(2, "0")}:${seconds.toString().padStart(2, "0")}`}
      </div>
    );
  }, [deadline, diffMs]);

  if (!deadline) {
    return null;
  }

  return (
    <div
      style={{
        userSelect: "none",
      }}
    >
      {displayed}
      <Toast position="bottom-right" ref={toast} />
    </div>
  );
};

export default Countdown;
