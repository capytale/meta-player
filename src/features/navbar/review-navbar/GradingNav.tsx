import { useState, useEffect, useMemo } from "react";
import evalApi from "@capytale/activity.js/backend/capytale/evaluation";
import { Dropdown } from "primereact/dropdown";

import { useAppSelector } from "../../../app/hooks";
import {
  selectActivityInfo,
  selectActivityNid,
  selectMode,
  selectNid,
} from "../../activityData/activityDataSlice";
import { studentNameFromInfo } from "../student-utils";

import { SaInfo } from "@capytale/activity.js/activity/evaluation/evaluation";
import { SelectItem, SelectItemOptionsType } from "primereact/selectitem";
import { Button } from "primereact/button";

const GradingNav: React.FC = () => {
  const activityInfo = useAppSelector(selectActivityInfo);
  const [studentList, setStudentList] = useState<SaInfo[]>([]);
  const nid = useAppSelector(selectNid) as number;
  const mode = useAppSelector(selectMode);
  const activityNid = useAppSelector(selectActivityNid) as number;
  useEffect(() => {
    evalApi.listSa(activityNid).then((j) => {
      setStudentList(j);
    });
  }, []);

  const goToStudent = (snid: string | number) => {
    const redir = new URL(window.location.href);
    redir.searchParams.set("id", snid.toString());
    redir.searchParams.set("mode", mode);
    location.href = redir.toString();
  };

  const handlePrev = () => {
    if (studentList == null) return;
    const i = studentList.findIndex((el) => (el.nid as number) == nid);
    if (i <= 0) return;
    goToStudent(studentList[i - 1].nid);
  };
  const handleNext = () => {
    if (studentList == null) return;
    const i = studentList.findIndex((el) => el.nid == nid);
    if (i < 0 || i >= studentList.length - 1) return;
    goToStudent(studentList[i + 1].nid);
  };

  function getWf(el: SaInfo) {
    if (el.workflow == "100") return { icon: "pi pi-pencil", color: "blue" };
    if (el.workflow == "200")
      return { icon: "pi pi-envelope", color: "orange" };
    if (el.workflow == "300")
      return { icon: "pi pi-list-check", color: "green" };
    throw new Error("Unknown workflow");
  }

  const options: SelectItemOptionsType = useMemo(() => {
    return studentList.map((el) => {
      const item: SelectItem = {
        label:
          studentNameFromInfo({
            firstName: el.firstname,
            lastName: el.lastname,
            class: el.classe,
          }) || undefined,
        value: el.nid,
        icon: getWf(el).icon,
      };
      return item;
    });
  }, [studentList]);

  if (studentList.length === 0) {
    return <span>{studentNameFromInfo(activityInfo.studentInfo)}</span>;
  }

  const firstNid = studentList[0].nid as number;
  const lastNid = studentList[Object.keys(studentList).length - 1]
    .nid as number;

  return (
    <div className="p-inputgroup flex-1">
      <Button
        aria-label="Passer à la copie précédente"
        size="small"
        icon="pi pi-chevron-left"
        severity="secondary"
        onClick={handlePrev}
        tooltip="Copie précédente"
        tooltipOptions={{
          position: "left",
        }}
        disabled={nid == firstNid}
        outlined
      />
      <Dropdown
        aria-label="Sélectionner une copie"
        value={nid}
        options={options}
        filter
        onChange={(e) => {
          goToStudent(e.value);
        }}
      />
      <Button
        aria-label="Passer à la copie suivante"
        size="small"
        icon="pi pi-chevron-right"
        severity="secondary"
        onClick={handleNext}
        tooltip="Copie suivante"
        tooltipOptions={{
          position: "right",
        }}
        disabled={nid == lastNid}
        outlined
      />
    </div>
  );
};

export default GradingNav;
