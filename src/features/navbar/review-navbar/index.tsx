import { FC } from "react";
import GradingNav from "./GradingNav";

import styles from "./style.module.scss";

const ReviewNavbar: FC = () => {
  return (
    <div className={styles.reviewNavbar}>
      <div aria-label="Indicateur de mode correction">Mode correction</div>
      <div>
        <GradingNav />
      </div>
      <div></div>
    </div>
  );
};

export default ReviewNavbar;
