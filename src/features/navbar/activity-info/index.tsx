import { useAppSelector } from "../../../app/hooks";
import {
  selectActivityInfo,
  selectIcon,
  selectMode,
} from "../../activityData/activityDataSlice";
import { studentNameFromInfo } from "../student-utils";
import styles from "./style.module.scss";

const ActivityInfo: React.FC = () => {
  const icon = useAppSelector(selectIcon);
  const mode = useAppSelector(selectMode);
  const activityInfo = useAppSelector(selectActivityInfo);
  const studentName = studentNameFromInfo(activityInfo.studentInfo);
  return (
    <div className={styles.activityInfo}>
      <div className={styles.activityInfoText}>
        <div
          className={styles.activityInfoTitle}
          aria-label="Titre de l'activité"
        >
          {activityInfo.title}
        </div>
        {mode === "assignment" && studentName && (
          <div
            className={styles.activityInfoStudentName}
            aria-label="Nom de l'élève"
          >
            {studentName}
          </div>
        )}
        {mode === "create" && (
          <div className={styles.activityInfoStudentName} aria-label="Mode">
            Création d'activité
          </div>
        )}
      </div>
      {icon?.path && (
        <img
          className={styles.activityLogo}
          aria-label="Logo de l'activité"
          src={icon.path}
          style={{
            backgroundColor: icon.style
              ? icon.style["background-color"] || "white"
              : "white",
          }}
        />
      )}
    </div>
  );
};

export default ActivityInfo;
