import { FC, useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import {
  QuickAction,
  selectSidebarActions,
  setSidebarActions,
  usePerformQuickAction,
} from "../navbarSlice";
import { Button } from "primereact/button";
import settings from "../../../settings";

const ActivitySidebarActions: FC<{}> = () => {
  const sidebarActions = useAppSelector(selectSidebarActions);
  const performQuickAction = usePerformQuickAction();
  return (
    <>
      {sidebarActions.map((action) => (
        <Button
          aria-label={action.title}
          key={action.title}
          severity="secondary"
          size="small"
          icon={action.icon}
          onClick={() => {
            performQuickAction(action);
          }}
          outlined
          label={action.title}
          tooltipOptions={{
            position: "left",
            showDelay: settings.TOOLTIP_SHOW_DELAY,
          }}
        />
      ))}
    </>
  );
};

type ActivitySidebarActionsSetterProps = {
  actions: QuickAction[];
};

export const ActivitySidebarActionsSetter: FC<
  ActivitySidebarActionsSetterProps
> = (props) => {
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(setSidebarActions(props.actions));
    return () => {
      dispatch(setSidebarActions([]));
    };
  }, [props.actions]);

  return <></>;
};

export default ActivitySidebarActions;
