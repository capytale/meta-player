import { FC } from "react";

import { Fieldset } from "primereact/fieldset";

import { selectOrientation, setLayout } from "../../layout/layoutSlice";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { RadioButton } from "primereact/radiobutton";

import styles from "./style.module.scss";
import {
  selectThemeNameOrAuto,
  setAutoSwitch,
  switchToTheme,
} from "../../theming/themingSlice";
import { selectSidebarActions } from "../navbarSlice";
import ActivitySidebarActions from "./ActivitySidebarActions";
import { classNames } from "primereact/utils";
import {
  selectCanChoosePedagoLayout,
  selectCanChooseTheme,
} from "../../activityData/activityDataSlice";
import { ActivitySettingsDisplay } from "../../activitySettings";
import { Button } from "primereact/button";
import { ConfirmPopup, confirmPopup } from "primereact/confirmpopup";
import settings from "../../../settings";
import { useReset } from "../../activityJS/hooks";

type SettingsSidebarContentProps = {
  showHelp?: () => void;
};

const SettingsSidebarContent: FC<SettingsSidebarContentProps> = (props) => {
  const canChooseOrientation = useAppSelector(selectCanChoosePedagoLayout);
  const canChooseTheme = useAppSelector(selectCanChooseTheme);
  const orientation = useAppSelector(selectOrientation);
  const themeName = useAppSelector(selectThemeNameOrAuto);
  const sidebarActions = useAppSelector(selectSidebarActions);
  const reset = useReset({ onError: console.error });
  const dispatch = useAppDispatch();
  return (
    <>
      <div className={styles.sidebarCapytaleActions}>
        {props.showHelp && (
          <Button
            aria-label="Voir la documentation"
            severity="secondary"
            size="small"
            icon={"pi pi-question-circle"}
            onClick={() => {
              props.showHelp!();
            }}
            outlined
            label="Documentation"
            tooltip="Voir la documentation"
            tooltipOptions={{
              position: "left",
              showDelay: settings.TOOLTIP_SHOW_DELAY,
            }}
          />
        )}
        {reset && (
          <>
            <ConfirmPopup />
            <Button
              aria-label="Réinitialiser l'activité"
              severity="secondary"
              size="small"
              icon={"pi pi-undo"}
              onClick={(event) => {
                confirmPopup({
                  target: event.currentTarget,
                  message:
                    "Toutes vos modifications seront perdues. Continuer ?",
                  icon: "pi pi-exclamation-triangle",
                  defaultFocus: "reject",
                  acceptClassName: "p-button-danger",
                  accept: reset,
                  acceptLabel: "Oui",
                  rejectLabel: "Non",
                });
              }}
              outlined
              label="Réinitialiser l'activité"
              tooltip="Revenir à la version de l'enseignant"
              tooltipOptions={{
                position: "left",
                showDelay: settings.TOOLTIP_SHOW_DELAY,
              }}
            />
          </>
        )}
      </div>
      {sidebarActions.length > 0 && (
        <Fieldset
          legend="Actions"
          className={classNames(
            "sidebarFieldset",
            styles.sidebarFieldsetButtons,
          )}
        >
          <ActivitySidebarActions />
        </Fieldset>
      )}
      {canChooseOrientation && (
        <Fieldset legend="Disposition" className="sidebarFieldset">
          <div className="sidebarRadioButtons">
            <div className="sidebarRadioGroup">
              <RadioButton
                aria-label="Option de disposition horizontale"
                inputId="rb-horizontal"
                name="horizontal"
                value="horizontal"
                onChange={(e) => dispatch(setLayout(e.value))}
                checked={orientation === "horizontal"}
              />
              <label htmlFor="rb-horizontal" className="ml-2">
                Horizontale
              </label>
            </div>
            <div className="sidebarRadioGroup">
              <RadioButton
                aria-label="Option de disposition verticale"
                inputId="rb-vertical"
                name="vertical"
                value="vertical"
                onChange={(e) => dispatch(setLayout(e.value))}
                checked={orientation === "vertical"}
              />
              <label htmlFor="rb-vertical" className="ml-2">
                Verticale
              </label>
            </div>
          </div>
        </Fieldset>
      )}
      {canChooseTheme && (
        <Fieldset legend="Thème" className="sidebarFieldset">
          <div className="sidebarRadioButtons">
            <div className="sidebarRadioGroup">
              <RadioButton
                aria-label="Option de thème clair"
                inputId="rb-light"
                name="light"
                value="light"
                onChange={(_e) => {
                  dispatch(setAutoSwitch(false));
                  dispatch(switchToTheme("light"));
                }}
                checked={themeName === "Clair"}
              />
              <label htmlFor="rb-light" className="ml-2">
                Clair
              </label>
            </div>
            <div className="sidebarRadioGroup">
              <RadioButton
                aria-label="Option de thème sombre"
                inputId="rb-dark"
                name="dark"
                value="dark"
                onChange={(_e) => {
                  dispatch(setAutoSwitch(false));
                  dispatch(switchToTheme("dark"));
                }}
                checked={themeName === "Sombre"}
              />
              <label htmlFor="rb-dark" className="ml-2">
                Sombre
              </label>
            </div>
            <div className="sidebarRadioGroup">
              <RadioButton
                aria-label="Option de thème système"
                inputId="rb-auto"
                name="auto"
                value="auto"
                onChange={(_e) => dispatch(setAutoSwitch(true))}
                checked={themeName === "Système (auto)"}
              />
              <label htmlFor="rb-auto" className="ml-2">
                Système (auto)
              </label>
            </div>
          </div>
        </Fieldset>
      )}
      <ActivitySettingsDisplay />
    </>
  );
};

export default SettingsSidebarContent;
