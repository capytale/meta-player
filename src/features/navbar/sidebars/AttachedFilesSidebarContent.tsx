import { FC, useRef } from "react";
import {
  useAttachedFiles,
  usePreviewImageFile,
  usePreviewTextFile,
} from "../../functionalities/hooks";
import { useAppSelector } from "../../../app/hooks";
import {
  AttachedFileData,
  selectAttachedFilesOptions,
} from "../../functionalities/functionalitiesSlice";
import { Button } from "primereact/button";
import { Toast } from "primereact/toast";

import styles from "./AttachedFilesSidebarContent.module.scss";
import { copyToClipboard } from "../../../utils/clipboard";
import { downloadFile } from "../../../utils/download";
import { useFileUpload } from "use-file-upload";
import mime from "mime";

const AttachedFilesSidebarContent: FC = () => {
  const filesData = useAttachedFiles();
  const attachedFilesOptions = useAppSelector(selectAttachedFilesOptions);
  const [_, selectFile] = useFileUpload();

  return (
    <div className={styles.sidebarContent}>
      <div className={styles.attachedFiles}>
        {filesData.map((file) => (
          <AttachedFileLinks key={file.name} fileData={file} />
        ))}
      </div>
      {attachedFilesOptions.uploadTemporaryFiles && (
        <div className={styles.uploadFilesButtonContainer}>
          <Button
            aria-label="Ajouter fichier temporaire"
            icon="pi pi-upload"
            label={
              attachedFilesOptions.uploadTemporaryFiles?.uploadButtonLabel ||
              "Ajouter fichier temporaire"
            }
            onClick={() => {
              selectFile(
                {
                  multiple: attachedFilesOptions.uploadTemporaryFiles?.multiple,
                  accept:
                    attachedFilesOptions.uploadTemporaryFiles?.mimeTypes?.join(
                      ",",
                    ) || undefined,
                },
                (filesOrFiles) => {
                  if (Array.isArray(filesOrFiles)) {
                    attachedFilesOptions.uploadTemporaryFiles?.handler(
                      filesOrFiles,
                    );
                  } else {
                    /* Deconstructing the object here so that if there is a
                  bug, the error will be caught here and not in the handler */
                    const { source, name, size, file } = filesOrFiles;
                    attachedFilesOptions.uploadTemporaryFiles?.handler([
                      { source, name, size, file },
                    ]);
                  }
                },
              );
            }}
            className={styles.uploadButton}
          />
        </div>
      )}
    </div>
  );
};

const AttachedFileLinks: FC<{ fileData: AttachedFileData }> = ({
  fileData,
}) => {
  const attachedFilesOptions = useAppSelector(selectAttachedFilesOptions);
  const toast = useRef<Toast>(null);
  const previewTextFile = usePreviewTextFile();
  const previewImageFile = usePreviewImageFile();

  return (
    <div className={styles.fileRow}>
      <Toast ref={toast} position="bottom-right" />
      <Button
        label={fileData.name}
        severity={fileData.isTemporary ? "warning" : "secondary"}
        disabled={fileData.interactMode === "none"}
        onClick={async () => {
          if (fileData.interactMode === "custom") {
            attachedFilesOptions.customHandlers?.interactWithFile?.(fileData);
          } else if (fileData.interactMode === "download") {
            window.open(fileData.urlOrId, "_blank")?.focus();
          } else if (fileData.interactMode === "preview") {
            const mimeType = mime.getType(fileData.name);
            if (
              mimeType?.startsWith("text") ||
              mimeType === "application/json"
            ) {
              // fetch file content
              const response = await fetch(fileData.urlOrId);
              const content = await response.text();
              previewTextFile(fileData.name, content);
            } else if (mimeType?.startsWith("image")) {
              previewImageFile(fileData.name, fileData.urlOrId);
            } else {
              toast.current?.show({
                severity: "info",
                summary: `Impossible de prévisualiser {fileData.name}`,
                detail: `Aperçu non disponible pour ce type de fichier (${mimeType})`,
                life: 3000,
              });
            }
          }
        }}
        className={styles.fileInteraction}
        text
      />
      {fileData.downloadMode !== "none" && (
        <Button
          icon="pi pi-download"
          severity="secondary"
          onClick={() => {
            if (fileData.downloadMode === "custom") {
              attachedFilesOptions.customHandlers?.downloadFile?.(fileData);
              return;
            } else if (fileData.downloadMode === "default") {
              downloadFile(fileData.urlOrId, fileData.name);
              toast.current?.show({
                severity: "success",
                summary: "Téléchargement lancé",
                detail: fileData.name,
                life: 3000,
              });
            }
          }}
          className={styles.fileSmallInteraction}
          text
        />
      )}
      {fileData.copyLinkMode !== "none" && (
        <Button
          icon="pi pi-link"
          severity="secondary"
          onClick={() => {
            if (fileData.copyLinkMode === "custom") {
              attachedFilesOptions.customHandlers?.copyFileLink?.(fileData);
              return;
            } else if (fileData.copyLinkMode === "default") {
              // copy link to clipboard
              copyToClipboard(fileData.urlOrId, (success, error) => {
                if (success) {
                  toast.current?.show({
                    severity: "success",
                    summary: "Lien copié",
                    detail: fileData.name,
                    life: 3000,
                  });
                } else {
                  toast.current?.show({
                    severity: "error",
                    summary: "Erreur lors de la copie du lien",
                    detail: error,
                    life: 5000,
                  });
                }
              });
            }
          }}
          style={{
            flexGrow: 0,
            flexShrink: 0,
          }}
          text
        />
      )}
    </div>
  );
};

export default AttachedFilesSidebarContent;
