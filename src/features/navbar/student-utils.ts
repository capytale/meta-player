import { StudentInfo } from "../activityData/activityJsData";

export function studentNameFromInfo(studentInfo: StudentInfo | null) {
  if (!studentInfo || (!studentInfo.firstName && !studentInfo.lastName)) {
    return null;
  }
  if (studentInfo.class && studentInfo.class !== "enseignants") {
    return `${studentInfo.firstName} ${studentInfo.lastName} — ${studentInfo.class}`;
  }
  return `${studentInfo.firstName} ${studentInfo.lastName}`;
}
