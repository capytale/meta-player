import { Fieldset } from "primereact/fieldset";
import { useActivitySettings } from "./hooks";
import {
  ActivitySettingsFormDisplay,
  ActivitySettingsOptionsDisplay,
} from "./ui";

type ActivitySettingsDisplayProps = {};

export function ActivitySettingsDisplay({}: ActivitySettingsDisplayProps) {
  const settings = useActivitySettings();
  if (!settings) return null;
  return (
    <div>
      {Object.keys(settings).map((id) => {
        const section = settings[id];
        return (
          <Fieldset key={id} legend={section.title} className="sidebarFieldset">
            {section.type === "form" ? (
              <ActivitySettingsFormDisplay form={section} sectionId={id} />
            ) : (
              <ActivitySettingsOptionsDisplay
                options={section}
                sectionId={id}
              />
            )}
          </Fieldset>
        );
      })}
    </div>
  );
}
