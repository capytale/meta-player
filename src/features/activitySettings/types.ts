export type ActivitySettingsSelect = {
  type: "select";
  options: {
    label: string;
    name: string;
  }[];
  selectedOptionName: string;
};

export type ActivitySettingsRange = {
  type: "range";
  min: number;
  max: number;
  step?: number;
  value: number;
};

export type ActivitySettingsTextArea = {
  type: "textarea";
  value: string;
};

export type ActivitySettingsOption = {
  type: "checkbox" | "switch";
  value: boolean;
};

export type ActivitySettingsInput = {
  type: "input";
  inputType:
    | "color"
    | "date"
    | "datetime-local"
    | "email"
    | "month"
    | "number"
    | "password"
    | "search"
    | "tel"
    | "text"
    | "time"
    | "url"
    | "week";
  value: string | number;
};

export type ActivitySettingsFormSection = {
  type: "form";
  fields: {
    [name: string]: (
      | ActivitySettingsRange
      | ActivitySettingsInput
      | ActivitySettingsTextArea
      | ActivitySettingsOption
      | ActivitySettingsSelect
    ) & {
      label: string;
    };
  };
};

export type ActivitySettingsMultipleOptionsSection = {
  type: "checkboxes" | "switches";
  options: {
    label: string;
    name: string;
  }[];
  selectedOptionNames: string[];
};

export type ActivitySettingsRadioOptionsSection = {
  type: "radio";
  options: {
    label: string;
    name: string;
  }[];
  selectedOptionName: string | null;
};

export type ActivitySettingsSection = (
  | ActivitySettingsMultipleOptionsSection
  | ActivitySettingsRadioOptionsSection
  | ActivitySettingsFormSection
) & {
  title: string;
};

export type ActivitySettings = { [id: string]: ActivitySettingsSection };
