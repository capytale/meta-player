import { useAppSelector } from "../../app/hooks";
import { selectSettings } from "./activitySettingsSlice";

export const useActivitySettings = () => {
  return useAppSelector(selectSettings);
};
