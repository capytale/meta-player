import type { PayloadAction } from "@reduxjs/toolkit";
import { createAppSlice } from "../../app/createAppSlice";
import { ActivitySettings } from "./types";

// If you are not using async thunks you can use the standalone `createSlice`.
export const activitySettingsSlice = createAppSlice({
  name: "activitySettings",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState: {
    settings: undefined as ActivitySettings | undefined,
  },
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: (create) => ({
    setSettings: create.reducer(
      (state, action: PayloadAction<ActivitySettings | undefined>) => {
        state.settings = action.payload;
      },
    ),
    updateSettings: create.reducer(
      (
        state,
        action: PayloadAction<
          (
            oldSettings: ActivitySettings | undefined,
          ) => ActivitySettings | undefined
        >,
      ) => {
        state.settings = action.payload(state.settings);
      },
    ),
  }),
  // You can define your selectors here. These selectors receive the slice
  // state as their first argument.
  selectors: {
    selectSettings: (data) => data.settings,
  },
});

// Action creators are generated for each case reducer function.
export const { setSettings, updateSettings } = activitySettingsSlice.actions;

// Selectors returned by `slice.selectors` take the root state as their first argument.
export const { selectSettings } = activitySettingsSlice.selectors;
