import type {
  ActivitySettings,
  ActivitySettingsFormSection,
  ActivitySettingsMultipleOptionsSection,
  ActivitySettingsRadioOptionsSection,
} from "./types";

import styles from "./style.module.scss";
import { Slider } from "primereact/slider";
import { InputNumber } from "primereact/inputnumber";
import { InputText } from "primereact/inputtext";
import { ColorPicker } from "primereact/colorpicker";
import { InputTextarea } from "primereact/inputtextarea";
import { Checkbox } from "primereact/checkbox";
import { InputSwitch } from "primereact/inputswitch";
import { Dropdown } from "primereact/dropdown";
import React from "react";
import { RadioButton } from "primereact/radiobutton";
import { useAppDispatch } from "../../app/hooks";
import { updateSettings } from "./activitySettingsSlice";

type ActivitySettingsFormDisplayProps = {
  form: ActivitySettingsFormSection;
  sectionId: string;
};

export function ActivitySettingsFormDisplay({
  form,
  sectionId,
}: ActivitySettingsFormDisplayProps) {
  const dispatch = useAppDispatch();
  const selectOnChange = (fieldName: string, value: string) => {
    dispatch(
      updateSettings((oldSettings: ActivitySettings | undefined) => {
        if (!oldSettings) {
          return undefined;
        }
        const oldSection = oldSettings[
          sectionId
        ] as ActivitySettingsFormSection;
        const newSettings = {
          ...oldSettings,
          [sectionId]: {
            ...oldSection,
            type: "form",
            fields: {
              ...oldSection.fields,
              [fieldName]: {
                ...oldSection.fields[fieldName],
                selectedOptionName: value,
              },
            },
          },
        };
        return newSettings as ActivitySettings;
      }),
    );
  };
  const onChange = (fieldName: string, value: string | number | boolean) => {
    dispatch(
      updateSettings((oldSettings: ActivitySettings | undefined) => {
        if (!oldSettings) {
          return undefined;
        }
        const oldSection = oldSettings[
          sectionId
        ] as ActivitySettingsFormSection;
        const newSettings = {
          ...oldSettings,
          [sectionId]: {
            ...oldSection,
            type: "form",
            fields: {
              ...oldSection.fields,
              [fieldName]: {
                ...oldSection.fields[fieldName],
                value,
              },
            },
          },
        };
        return newSettings as ActivitySettings;
      }),
    );
  };
  return (
    <div className="mb-3">
      {Object.keys(form.fields).map((name, _) => {
        const field = form.fields[name];
        switch (field.type) {
          case "range":
            return (
              <div className={styles.formGroup}>
                <label
                  className={styles.formLabel}
                  htmlFor={sectionId + "-" + name}
                >
                  Range
                </label>
                <InputNumber
                  min={field.min}
                  max={field.max}
                  step={field.step}
                  value={field.value}
                  onChange={(e) => onChange(name, e.value as number)}
                />
                <Slider
                  id={sectionId + "-" + name}
                  min={field.min}
                  max={field.max}
                  step={field.step}
                  value={field.value}
                  onChange={(e) => onChange(name, e.value as number)}
                />
              </div>
            );
          case "input":
            return (
              <div className={styles.formGroup}>
                <label
                  className={styles.formLabel}
                  htmlFor={sectionId + "-" + name}
                >
                  {field.label}
                </label>
                {field.inputType === "text" && (
                  <InputText
                    id={sectionId + "-" + name}
                    value={field.value as string}
                    onChange={(e) => onChange(name, e.target.value)}
                  />
                )}
                {field.inputType === "number" && (
                  <InputNumber
                    inputId={sectionId + "-" + name}
                    value={field.value as number}
                    onChange={(e) => e.value != null && onChange(name, e.value)}
                  />
                )}
                {field.inputType === "color" && (
                  <ColorPicker
                    inputId={sectionId + "-" + name}
                    value={field.value as string}
                    onChange={(e) =>
                      e.value && onChange(name, e.value as string)
                    }
                  />
                )}
                {field.inputType !== "color" &&
                  field.inputType !== "number" &&
                  field.inputType !== "text" && (
                    <p>Type d'input {field.inputType} pas implémenté.</p>
                  )}
              </div>
            );
          case "textarea":
            return (
              <div className={styles.formGroup}>
                <label
                  className={styles.formLabel}
                  htmlFor={sectionId + "-" + name}
                >
                  {field.label}
                </label>
                <InputTextarea
                  id={sectionId + "-" + name}
                  value={field.value}
                  onChange={(e) => onChange(name, e.target.value)}
                />
              </div>
            );
          case "checkbox":
            return (
              <CheckboxGroup
                inputId={sectionId + "-" + name}
                checked={field.value}
                onChange={(value) => onChange(name, value)}
                label={field.label}
              />
            );
          case "switch":
            return (
              <SwitchGroup
                inputId={sectionId + "-" + name}
                checked={field.value}
                onChange={(value) => onChange(name, value)}
                label={field.label}
              />
            );
          case "select":
            return (
              <div className={styles.formGroup}>
                <label
                  className={styles.formLabel}
                  htmlFor={sectionId + "-" + name}
                >
                  {field.label}
                </label>
                <Dropdown
                  inputId={sectionId + "-" + name}
                  value={field.selectedOptionName}
                  onChange={(e) => selectOnChange(name, e.value)}
                  options={field.options}
                  optionLabel="label"
                  optionValue="name"
                />
              </div>
            );
        }
      })}
    </div>
  );
}

type ActivitySettingsOptionsDisplayProps = {
  options:
    | ActivitySettingsMultipleOptionsSection
    | ActivitySettingsRadioOptionsSection;
  sectionId: string;
};

export function ActivitySettingsOptionsDisplay({
  options,
  sectionId,
}: ActivitySettingsOptionsDisplayProps) {
  return (
    <div>
      {options.type === "radio" ? (
        <ActivitySettingsRadioDisplay options={options} sectionId={sectionId} />
      ) : (
        <ActivitySettingsCheckboxesDisplay
          options={options}
          sectionId={sectionId}
        />
      )}
    </div>
  );
}

type ActivitySettingsCheckboxesDisplayProps = {
  options: ActivitySettingsMultipleOptionsSection;
  sectionId: string;
};

function ActivitySettingsCheckboxesDisplay({
  options,
  sectionId,
}: ActivitySettingsCheckboxesDisplayProps) {
  const dispatch = useAppDispatch();
  const onChange = (optionName: string, checked: boolean) => {
    dispatch(
      updateSettings((oldSettings: ActivitySettings | undefined) => {
        if (!oldSettings) {
          return undefined;
        }
        const oldSection = oldSettings[
          sectionId
        ] as ActivitySettingsMultipleOptionsSection;
        const newSettings = {
          ...oldSettings,
          [sectionId]: {
            ...oldSection,
            selectedOptionNames: checked
              ? [...oldSection.selectedOptionNames, optionName]
              : oldSection.selectedOptionNames.filter((n) => n !== optionName),
          },
        };
        return newSettings as ActivitySettings;
      }),
    );
  };
  return (
    <div className="sidebarRadioButtons">
      {options.options.map((option) => (
        <CheckboxGroup
          key={option.name}
          inputId={sectionId + "-" + option.name}
          checked={options.selectedOptionNames.includes(option.name)}
          onChange={(value) => onChange(option.name, value)}
          label={option.label}
        />
      ))}
    </div>
  );
}

type ActivitySettingsRadioDisplayProps = {
  options: ActivitySettingsRadioOptionsSection;
  sectionId: string;
};

function ActivitySettingsRadioDisplay({
  options,
  sectionId,
}: ActivitySettingsRadioDisplayProps) {
  const dispatch = useAppDispatch();
  const onChange = (optionName: string, checked: boolean) => {
    if (!checked) {
      return;
    }
    dispatch(
      updateSettings((oldSettings: ActivitySettings | undefined) => {
        if (!oldSettings) {
          return undefined;
        }
        const oldSection = oldSettings[
          sectionId
        ] as ActivitySettingsRadioOptionsSection;
        const newSettings = {
          ...oldSettings,
          [sectionId]: {
            ...oldSection,
            selectedOptionName: checked ? optionName : null,
          },
        };
        return newSettings as ActivitySettings;
      }),
    );
  };
  return (
    <div className="sidebarRadioButtons">
      {options.options.map((option) => (
        <RadioGroup
          key={option.name}
          inputId={sectionId + "-" + option.name}
          checked={options.selectedOptionName === option.name}
          onChange={(value) => {
            onChange(option.name, value);
          }}
          label={option.label}
        />
      ))}
    </div>
  );
}

type CheckboxGroupProps = {
  inputId: string;
  label: string;
  checked: boolean;
  onChange?: (value: boolean) => void;
};

const CheckboxGroup: React.FC<CheckboxGroupProps> = (props) => {
  return (
    <div className="sidebarRadioGroup">
      <Checkbox
        inputId={props.inputId}
        checked={props.checked}
        onChange={(e) =>
          props.onChange && e.checked != null && props.onChange(e.checked)
        }
      />
      <label className={styles.checkboxLabel} htmlFor={props.inputId}>
        {props.label}
      </label>
    </div>
  );
};

const RadioGroup: React.FC<CheckboxGroupProps> = (props) => {
  return (
    <div className="sidebarRadioGroup">
      <RadioButton
        inputId={props.inputId}
        checked={props.checked}
        onChange={(e) => {
          props.onChange && e.checked != null && props.onChange(e.checked);
        }}
      />
      <label className={styles.checkboxLabel} htmlFor={props.inputId}>
        {props.label}
      </label>
    </div>
  );
};

const SwitchGroup: React.FC<CheckboxGroupProps> = (props) => {
  return (
    <div className="sidebarRadioGroup">
      <InputSwitch
        inputId={props.inputId}
        checked={props.checked}
        onChange={(e) =>
          props.onChange && e.checked != null && props.onChange(e.checked)
        }
      />
      <label className={styles.checkboxLabel} htmlFor={props.inputId}>
        {props.label}
      </label>
    </div>
  );
};
