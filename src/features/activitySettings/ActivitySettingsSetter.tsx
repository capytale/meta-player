import { FC, useEffect, useRef } from "react";
import { useAppDispatch } from "../../app/hooks";
import { ActivitySettings } from "./types";
import { setSettings } from "./activitySettingsSlice";
import { deepEqual } from "../../utils/equality";

type ActivitySettingsSetterProps = {
  settings: ActivitySettings;
};

const ActivitySettingsSetter: FC<ActivitySettingsSetterProps> = ({
  settings,
}) => {
  const oldSettings = useRef<ActivitySettings | null>(null);
  const dispatch = useAppDispatch();
  useEffect(() => {
    if (deepEqual(oldSettings.current, settings)) {
      return;
    }
    oldSettings.current = settings;
    dispatch(setSettings(settings));
  }, [dispatch, settings]);
  return null;
};

export default ActivitySettingsSetter;
