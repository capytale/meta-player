import { FC, useEffect } from "react";
import { setIsPlayerDirty } from "./activityDataSlice";
import { useAppDispatch } from "../../app/hooks";

type IsDirtySetterProps = {
  isDirty: boolean;
};

const IsDirtySetter: FC<IsDirtySetterProps> = (props) => {
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(setIsPlayerDirty(props.isDirty));
  }, [props]);
  return null;
};

export default IsDirtySetter;
