import { ActivityMode } from "@capytale/activity.js/activity/activitySession";
import { TimeRange } from "@capytale/activity.js/common/field";
import { InitialEditorStateType } from "@capytale/capytale-rich-text-editor";
import { wf } from "@capytale/activity.js/activity/field/workflow";

export type StudentInfo = {
  firstName: string;
  lastName: string;
  class: string;
};

export type Instructions = {
  value: InitialEditorStateType | null;
  htmlValue: string | null;
  format: "html" | "markdown" | "lexical";
};

export type Icon = {
  path: string;
  style?: any;
};

export type EditorType = "rich" | "none";

export type ActivityJSData = {
  title: string;
  mode: ActivityMode;
  returnUrl: string;
  helpUrl: string;
  code: string | null;
  nid: number;
  activityNid: number;
  accessTrMode: string;
  accessTimerange: TimeRange | null;
  studentInfo: StudentInfo | null;
  instructions: Instructions | null;
  instructionsType: EditorType;
  pdfInstructions: Blob | null;
  sharedNotesContent: InitialEditorStateType | null;
  sharedNotesType: EditorType;
  codeLink: string | null;
  icon: Icon | null;
  friendlyType: string;
  comments: string | null;
  grading: string | null;
  workflow: wf | null | undefined;
  antiCheat?: null | {
    passwordHash?: string | null;
    startLocked: boolean;
  };
  hasEvaluations: boolean | null;
}

export const defaultActivityJSData: ActivityJSData = {
  title: "",
  mode: "view",
  returnUrl: "",
  helpUrl: "",
  code: null,
  nid: 0,
  activityNid: 0,
  accessTrMode: "",
  accessTimerange: null,
  studentInfo: {
    firstName: "",
    lastName: "",
    class: "",
  },
  instructions: null,
  instructionsType: "rich",
  pdfInstructions: null,
  sharedNotesContent: null,
  sharedNotesType: "none",
  codeLink: null,
  icon: null,
  friendlyType: "",
  comments: null,
  grading: null,
  workflow: null,
  hasEvaluations: null,
};