import type { PayloadAction } from "@reduxjs/toolkit";
import { createAppSlice } from "../../app/createAppSlice";
import { InitialEditorStateType } from "@capytale/capytale-rich-text-editor";
import {
  MetaPlayerOptions,
  defaultMetaPlayerOptions,
} from "./metaPlayerOptions";
import { wf } from "@capytale/activity.js/activity/field/workflow";
import { ActivityJSData, defaultActivityJSData, EditorType } from "./activityJsData";
import { defaultUIState, SaveState, UIState } from "./uiState";



export type ActivityDataState = ActivityJSData & MetaPlayerOptions & UIState;

const initialState: ActivityDataState = {
  ...defaultActivityJSData,
  ...defaultMetaPlayerOptions,
  ...defaultUIState,
};

// If you are not using async thunks you can use the standalone `createSlice`.
export const activityDataSlice = createAppSlice({
  name: "activityData",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: (create) => ({
    toRemove: create.reducer((state) => {
      state.hasInstructions = false;
    }),
    // Use the `PayloadAction` type to declare the contents of `action.payload`
    setActivityJSData: create.reducer(
      (state, action: PayloadAction<ActivityJSData>) => {
        state.title = action.payload.title;
        state.mode = action.payload.mode;
        state.returnUrl = action.payload.returnUrl;
        state.helpUrl = action.payload.helpUrl;
        state.code = action.payload.code;
        state.accessTrMode = action.payload.accessTrMode;
        state.accessTimerange = action.payload.accessTimerange;
        state.studentInfo = action.payload.studentInfo;
        state.instructions = action.payload.instructions;
        state.instructionsType = action.payload.instructionsType;
        state.sharedNotesContent = action.payload.sharedNotesContent;
        state.sharedNotesType = action.payload.sharedNotesType;
        state.codeLink = action.payload.codeLink;
        state.icon = action.payload.icon;
        state.friendlyType = action.payload.friendlyType;
        state.comments = action.payload.comments;
        state.grading = action.payload.grading;
        state.nid = action.payload.nid;
        state.activityNid = action.payload.activityNid;
        state.workflow = action.payload.workflow;
        state.antiCheat = action.payload.antiCheat;
        state.hasEvaluations = action.payload.hasEvaluations;
      },
    ),
    setPlayerSettings: create.reducer(
      (state, action: PayloadAction<MetaPlayerOptions>) => {
        state.hasInstructions = action.payload.hasInstructions;
        state.canReset = action.payload.canReset;
        state.pedagoLayout = action.payload.pedagoLayout;
        state.supportsLightTheme = action.payload.supportsLightTheme;
        state.supportsDarkTheme = action.payload.supportsDarkTheme;
        state.preventEditIfHasEvaluations = action.payload.preventEditIfHasEvaluations;
      },
    ),
    setCanSaveInstructions: create.reducer(
      (state, action: PayloadAction<boolean>) => {
        state.canSaveInstructions = action.payload;
      },
    ),
    setCanSaveSharedNotes: create.reducer(
      (state, action: PayloadAction<boolean>) => {
        state.canSaveSharedNotes = action.payload;
      },
    ),
    setLexicalInstructionsState: create.reducer(
      (state, action: PayloadAction<InitialEditorStateType>) => {
        state.instructions = {
          format: "lexical",
          value: action.payload,
          htmlValue: null,
        };
      },
    ),
    setLexicalSharedNotesState: create.reducer(
      (state, action: PayloadAction<InitialEditorStateType>) => {
        state.sharedNotesContent = action.payload;
      },
    ),
    setPdfInstructions: create.reducer(
      (state, action: PayloadAction<Blob | null>) => {
        state.pdfInstructions = action.payload;
      },
    ),
    setInstructionsType: create.reducer(
      (state, action: PayloadAction<EditorType>) => {
        state.instructionsType = action.payload;
      },
    ),
    setSharedNotesType: create.reducer(
      (state, action: PayloadAction<EditorType>) => {
        state.sharedNotesType = action.payload;
      },
    ),
    setSaveState: create.reducer((state, action: PayloadAction<SaveState>) => {
      state.saveState = action.payload;
    }),
    setGrading: create.reducer((state, action: PayloadAction<string>) => {
      state.grading = action.payload;
    }),
    setComments: create.reducer((state, action: PayloadAction<string>) => {
      state.comments = action.payload;
    }),
    setWorkflow: create.reducer((state, action: PayloadAction<wf>) => {
      state.workflow = action.payload;
    }),
    setIsPlayerDirty: create.reducer(
      (state, action: PayloadAction<boolean>) => {
        state.isPlayerDirty = action.payload;
      },
    ),
    setIsMPDirty: create.reducer((state, action: PayloadAction<boolean>) => {
      state.isMPDirty = action.payload;
    }),
    setIsAntiCheatExitDetectionDisabled: create.reducer(
      (state, action: PayloadAction<boolean>) => {
        state.isAntiCheatExitDetectionDisabled = action.payload;
      },
    ),
  }),
  // You can define your selectors here. These selectors receive the slice
  // state as their first argument.
  selectors: {
    selectMode: (data) => data.mode,
    selectActivityInfo: (data) => ({
      title: data.title,
      studentInfo: data.studentInfo,
    }),
    selectSharingInfo: (data) => ({
      code: data.code,
      codeLink: data.codeLink,
    }),
    selectInstructions: (data) => data.instructions,
    selectInstructionsType: (data) => data.instructionsType,
    selectPdfInstructions: (data) => data.pdfInstructions,
    selectSharedNotesContent: (data) => data.sharedNotesContent,
    selectSharedNotesType: (data) => data.sharedNotesType,
    selectComments: (data) => data.comments,
    selectGrading: (data) => data.grading,
    selectHasInstructions: (data) => data.hasInstructions,
    selectCanReset: (data) => data.canReset,
    selectPedagoLayout: (data) => data.pedagoLayout,
    selectHasGradingOrComments: (data) => !!(data.grading || data.comments),
    selectPreventEditIfHasEvaluations: (data) => data.preventEditIfHasEvaluations,
    selectSaveState: (data) => data.saveState,
    selectReturnUrl: (data) => data.returnUrl,
    selectNid: (data) => data.nid,
    selectActivityNid: (data) => data.activityNid,
    selectIcon: (data) => data.icon,
    selectWorkflow: (data) => data.workflow,
    selectIsPlayerDirty: (data) => data.isPlayerDirty,
    selectIsMPDirty: (data) => data.isMPDirty,
    selectIsDirty: (data) => data.isPlayerDirty || data.isMPDirty,

    selectCanChoosePedagoLayout: (data) =>
      data.pedagoLayout === "default-horizontal" ||
      data.pedagoLayout === "default-vertical",

    selectCanChooseTheme: (data) =>
      data.supportsDarkTheme && data.supportsLightTheme,

    selectShowSaveButton: (data) =>
      data.mode === "create" ||
      data.mode === "review" ||
      (data.mode === "assignment" && data.workflow === "current"),

    selectAntiCheat: (data) => data.antiCheat,
    selectHasAntiCheat: (data) =>
      !!(
        data.antiCheat &&
        data.antiCheat.passwordHash &&
        data.mode === "assignment" &&
        data.workflow === "current"
      ),
    selectIsAntiCheatExitDetectionDisabled: (data) =>
      data.isAntiCheatExitDetectionDisabled,
    selectHasEvaluations: (data) => data.hasEvaluations,
  },
});

// Action creators are generated for each case reducer function.
export const {
  toRemove,
  setActivityJSData,
  setPlayerSettings,
  setCanSaveInstructions,
  setCanSaveSharedNotes,
  setLexicalInstructionsState,
  setLexicalSharedNotesState,
  setPdfInstructions,
  setInstructionsType,
  setSharedNotesType,
  setSaveState,
  setGrading,
  setComments,
  setWorkflow,
  setIsPlayerDirty,
  setIsMPDirty,
  setIsAntiCheatExitDetectionDisabled,
} = activityDataSlice.actions;

// Selectors returned by `slice.selectors` take the root state as their first argument.
export const {
  selectMode,
  selectActivityInfo,
  selectSharingInfo,
  selectInstructions,
  selectInstructionsType,
  selectPdfInstructions,
  selectSharedNotesContent,
  selectSharedNotesType,
  selectComments,
  selectGrading,
  selectHasInstructions,
  selectCanReset,
  selectPedagoLayout,
  selectHasGradingOrComments,
  selectPreventEditIfHasEvaluations,
  selectSaveState,
  selectReturnUrl,
  selectNid,
  selectActivityNid,
  selectIcon,
  selectWorkflow,
  selectIsPlayerDirty,
  selectIsMPDirty,
  selectIsDirty,

  selectCanChoosePedagoLayout,
  selectCanChooseTheme,
  selectShowSaveButton,

  selectAntiCheat,
  selectHasAntiCheat,
  selectIsAntiCheatExitDetectionDisabled,
  selectHasEvaluations,
} = activityDataSlice.selectors;
