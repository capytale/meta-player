import { FC, useEffect } from "react";
import { useAppDispatch } from "../../app/hooks";
import { setPlayerSettings } from "./activityDataSlice";
import { setLayout } from "../layout/layoutSlice";
import { setAutoSwitch, switchToTheme } from "../theming/themingSlice";
import {
  defaultMetaPlayerOptions,
  MetaPlayerOptions,
} from "./metaPlayerOptions";

type MetaPlayerOptionsSetterProps = {
  options: Partial<MetaPlayerOptions>;
};

const MetaPlayerOptionsSetter: FC<MetaPlayerOptionsSetterProps> = (props) => {
  const dispatch = useAppDispatch();
  useEffect(() => {
    const options = { ...defaultMetaPlayerOptions, ...props.options };
    if (!options.supportsLightTheme && !options.supportsDarkTheme) {
      throw new Error("At least one theme must be supported");
    }
    dispatch(setPlayerSettings(options));
    dispatch(
      setLayout(
        options.pedagoLayout === "horizontal" ||
          options.pedagoLayout === "default-horizontal"
          ? "horizontal"
          : "vertical",
      ),
    );
    if (options.supportsDarkTheme && options.supportsLightTheme) {
      dispatch(setAutoSwitch(true));
    } else {
      dispatch(setAutoSwitch(false));
      dispatch(switchToTheme(options.supportsLightTheme ? "light" : "dark"));
    }
  }, [dispatch, props.options]);
  return null;
};

export default MetaPlayerOptionsSetter;
