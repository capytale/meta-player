export type MetaPlayerOptions = {
  hasInstructions: boolean;
  canReset: boolean;
  preventEditIfHasEvaluations: boolean;
  pedagoLayout:
  | "horizontal"
  | "vertical"
  | "default-horizontal"
  | "default-vertical";
  supportsLightTheme: boolean;
  supportsDarkTheme: boolean;
};

export const defaultMetaPlayerOptions: MetaPlayerOptions = {
  hasInstructions: true,
  canReset: true,
  preventEditIfHasEvaluations: false,
  pedagoLayout: "default-horizontal",
  supportsLightTheme: true,
  supportsDarkTheme: false,
};
