import { FC, useEffect } from "react";
import { useCanSave } from "./hooks";
import { useAppSelector } from "../../app/hooks";
import { selectIsDirty } from "./activityDataSlice";

const ExitWarning: FC = () => {
  const canSave = useCanSave();
  const isDirty = useAppSelector(selectIsDirty);

  useEffect(() => {
    const handleBeforeUnload = (e: BeforeUnloadEvent) => {
      if (canSave && isDirty) {
        e.preventDefault();
        e.returnValue = true;
      }
    };

    window.addEventListener("beforeunload", handleBeforeUnload);

    return () => {
      window.removeEventListener("beforeunload", handleBeforeUnload);
    };
  }, [canSave, isDirty]);

  return null;
};

export default ExitWarning;
