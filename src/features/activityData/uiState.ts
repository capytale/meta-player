export type SaveState = "idle" | "should-save" | "saving";

export type UIState = {
  canSaveInstructions: boolean;
  canSaveSharedNotes: boolean;
  saveState: SaveState;
  isPlayerDirty: boolean;
  isMPDirty: boolean;
  
  isAntiCheatExitDetectionDisabled: boolean;
}

export const defaultUIState: UIState = {
  canSaveInstructions: true,
  canSaveSharedNotes: true,
  saveState: "idle",
  isPlayerDirty: false,
  isMPDirty: false,

  isAntiCheatExitDetectionDisabled: false,
};