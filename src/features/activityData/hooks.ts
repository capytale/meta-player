import { useCallback } from "react";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { selectHasEvaluations, selectMode, selectPreventEditIfHasEvaluations, setIsPlayerDirty, setSaveState } from "./activityDataSlice";

export const useNotifyIsDirty = () => {
  const dispatch = useAppDispatch();
  return (isDirty: boolean = true) => dispatch(setIsPlayerDirty(isDirty));
};

export const useCanSave = () => {
  const preventEditIfHasEvaluations = useAppSelector(
    selectPreventEditIfHasEvaluations,
  );
  const hasEvaluations = useAppSelector(selectHasEvaluations);

  const mode = useAppSelector(selectMode);

  if (mode === "create" && hasEvaluations && preventEditIfHasEvaluations) {
    return false;
  }

  return true;
}

export const useSave = () => {
  const dispatch = useAppDispatch();
  const canSave = useCanSave();
  const setShouldSave = useCallback(() => {
    if (canSave) {
      dispatch(setSaveState("should-save"))
    }
  }, [dispatch, canSave]);
  return setShouldSave;
};
