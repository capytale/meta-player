import { FC, useEffect } from "react";
import { setIsAntiCheatExitDetectionDisabled } from "./activityDataSlice";
import { useAppDispatch } from "../../app/hooks";

type IsAntiCheatExitDetectionDisabledSetterProps = {
  isDisabled: boolean;
};

const IsAntiCheatExitDetectionDisabledSetter: FC<
  IsAntiCheatExitDetectionDisabledSetterProps
> = (props) => {
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(setIsAntiCheatExitDetectionDisabled(props.isDisabled));
  }, [props]);
  return null;
};

export default IsAntiCheatExitDetectionDisabledSetter;
