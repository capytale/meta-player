import { Panel } from "primereact/panel";
import styles from "./style.module.scss";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import {
  selectIsGradingVisible,
  selectOrientation,
  selectPedagoTab,
} from "../layout/layoutSlice";

import "@capytale/capytale-rich-text-editor/style.css";
import { Splitter, SplitterPanel } from "primereact/splitter";
import { classNames } from "primereact/utils";
import InstructionsEditor from "./InstructionsEditor";
import {
  selectComments,
  selectGrading,
  selectHasGradingOrComments,
  selectHasInstructions,
  selectMode,
  setComments,
  setGrading,
  setIsMPDirty,
} from "../activityData/activityDataSlice";
import { ChangeEventHandler, FC } from "react";
import { DivProps } from "react-html-props";
import SharedNotesEditor from "./SharedNotesEditor";
import { CloseOnlyPedagoCommands, PedagoCommands } from "./PedagoCommands";
import { PdfEditor } from "./PdfEditor";

const Pedago: React.FC<DivProps> = ({ className, ...props }) => {
  const mode = useAppSelector(selectMode);
  const pedagoTab = useAppSelector(selectPedagoTab);
  const hasInstructions = useAppSelector(selectHasInstructions);
  const hasGradingOrComments = useAppSelector(selectHasGradingOrComments);
  const isGradingVisible =
    useAppSelector(selectIsGradingVisible) &&
    (hasGradingOrComments || mode === "review");
  const isHorizontal = useAppSelector(selectOrientation) === "horizontal";
  const mayReverse = isHorizontal
    ? (tab: Array<any>) => {
        const reversed = [...tab];
        reversed.reverse();
        return reversed;
      }
    : (tab: Array<any>) => tab;

  if (!hasInstructions) {
    return (
      <div className={styles.onlyGradingPedago}>
        <CloseOnlyPedagoCommands />
        <div className={styles.gradingPanel}>
          <Grading />
        </div>
      </div>
    );
  }

  return (
    // @ts-ignore - Incompatibility for props in TS
    <div className={classNames(styles.pedago, className)} {...props}>
      <PedagoCommands />
      <div
        className={styles.pedagoContent}
        aria-label={
          pedagoTab === "instructions"
            ? "Consignes"
            : pedagoTab === "pdf"
              ? "PDF"
              : "Notes partagées"
        }
        data-grading-visible={isGradingVisible}
      >
        {!isGradingVisible && (
          <>
            {pedagoTab === "instructions" && <InstructionsEditor />}
            {pedagoTab === "sharedNotes" && <SharedNotesEditor />}
            {pedagoTab === "pdf" && <PdfEditor />}
          </>
        )}
        {isGradingVisible && (
          <Splitter
            layout={isHorizontal ? "horizontal" : "vertical"}
            className={styles.pedagoSplitter}
          >
            {mayReverse([
              <SplitterPanel
                aria-label="Panneau de notation"
                key="gradingPanel"
                minSize={30}
                size={40}
                className={styles.gradingPanel}
              >
                <Grading />
              </SplitterPanel>,
              <SplitterPanel
                aria-label="Panneau de consignes"
                key="pedagoPanel"
                minSize={50}
                size={60}
                className={styles.pedagoPanel}
              >
                <Panel
                  className={styles.fullSizePanel}
                  aria-label={
                    pedagoTab === "instructions"
                      ? "Consignes"
                      : pedagoTab === "pdf"
                        ? "PDF"
                        : "Notes partagées"
                  }
                  header={
                    pedagoTab === "instructions"
                      ? "Consignes"
                      : pedagoTab === "pdf"
                        ? "PDF"
                        : "Notes partagées"
                  }
                >
                  {pedagoTab === "instructions" && <InstructionsEditor />}
                  {pedagoTab === "sharedNotes" && <SharedNotesEditor />}
                  {pedagoTab === "pdf" && <PdfEditor />}
                </Panel>
              </SplitterPanel>,
            ])}
          </Splitter>
        )}
      </div>
    </div>
  );
};

const Grading: FC = () => {
  const mode = useAppSelector(selectMode);
  const comments = useAppSelector(selectComments);
  const grading = useAppSelector(selectGrading);
  const dispatch = useAppDispatch();

  const handleCommentsChange: ChangeEventHandler<HTMLTextAreaElement> = (
    event,
  ) => {
    dispatch(setComments(event.target.value));
    dispatch(setIsMPDirty(true));
  };

  const handleGradingChange: ChangeEventHandler<HTMLTextAreaElement> = (
    event,
  ) => {
    dispatch(setGrading(event.target.value));
    dispatch(setIsMPDirty(true));
  };

  return (
    <>
      <Panel
        className={classNames(styles.fullSizePanel, styles.pedagoFeedbackPanel)}
        header="Appréciation"
      >
        <div className={styles.pedagoFeedback}>
          {(comments || mode !== "assignment") && (
            <textarea
              aria-label="Saisie de l'appréciation"
              value={comments || ""}
              placeholder={
                mode !== "review" ? "" : "Rédigez ici l'appréciation."
              }
              id="comments"
              name="comments"
              rows={2}
              readOnly={mode !== "review"}
              onChange={handleCommentsChange}
              className={styles.fullTextarea}
            />
          )}
        </div>
      </Panel>
      <Panel
        className={classNames(styles.fullSizePanel, styles.pedagoGradePanel)}
        header="Évaluation"
      >
        <div className={styles.pedagoGrade}>
          {(grading || mode !== "assignment") && (
            <textarea
              aria-label="Saisie de l'évaluation"
              value={grading || ""}
              placeholder={
                mode !== "review"
                  ? ""
                  : "Écrivez ici l'évaluation libre (chiffrée ou non)."
              }
              id="grading"
              name="grading"
              rows={1}
              readOnly={mode !== "review"}
              onChange={handleGradingChange}
              className={styles.fullTextarea}
            />
          )}
        </div>
      </Panel>
    </>
  );
};

export default Pedago;
