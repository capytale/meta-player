import { useCapytaleRichTextEditor } from "@capytale/capytale-rich-text-editor";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import {
  selectSharedNotesContent,
  selectMode,
  setCanSaveSharedNotes,
  setIsMPDirty,
  setLexicalSharedNotesState,
  selectWorkflow,
  selectSharedNotesType,
  setSharedNotesType,
} from "../activityData/activityDataSlice";
import { forwardRef, useImperativeHandle, useRef, useState } from "react";
import { Toast } from "primereact/toast";
import settings from "../../settings";
import { Button } from "primereact/button";
import { Dialog } from "primereact/dialog";

const SharedNotesEditor: React.FC = forwardRef((_props, ref) => {
  const [Editor, getState, _canSave] = useCapytaleRichTextEditor();
  const dispatch = useAppDispatch();
  const toast = useRef<Toast>(null);
  const mode = useAppSelector(selectMode);
  const workflow = useAppSelector(selectWorkflow);
  const isEditable =
    mode === "create" ||
    (mode === "assignment" && workflow === "current") ||
    mode === "review";
  const initialEditorState = useAppSelector(selectSharedNotesContent);
  const initialStateOnChangeDone = useRef<boolean>(false);
  const sharedNotesType = useAppSelector(selectSharedNotesType);

  const [helpDialogVisible, setHelpDialogVisible] = useState(false);

  useImperativeHandle(ref, () => {
    return {
      save: async () => {
        const state = await getState();
        dispatch(setLexicalSharedNotesState(state.json));
      },
    };
  });

  return (
    <>
      <Toast position="bottom-right" ref={toast} />
      <>
        {sharedNotesType === "none" && (
          <>
            <p>Pas de notes partagées.</p>
            {mode === "create" && (
              <Button
                label="Créer des notes partagées"
                onClick={() => dispatch(setSharedNotesType("rich"))}
              />
            )}
            <div style={{ textAlign: "start", padding: "16px" }}>
              <h3>Que sont les notes partagées ?</h3>
              <p>
                Les notes partagées permettent aux élèves de communiquer avec
                leur enseignant. L'enseignant donne le contenu initial des notes
                partagées, et les élèves peuvent le modifier, notamment pour
                répondre à des questions, expliciter une démarche ou faire des
                remarques sur leur travail. L'enseignant peut également modifier
                les notes partagées pour répondre aux élèves.
              </p>
              <p>
                Les notes partagées peuvent contenir du texte, des images, des
                liens, etc., tout comme les consignes. La différence principale
                avec les consignes est que les élèves peuvent modifier le
                contenu des notes partagées. Elles sont un outil de
                communication supplémentaire entre l'enseignant et les élèves.
              </p>
            </div>
            <Button
              severity="secondary"
              size="small"
              icon={"pi pi-question-circle"}
              onClick={() => {
                setHelpDialogVisible(true);
              }}
              outlined
              label="En savoir plus"
              style={{ marginBottom: "16px" }}
            />
            <Dialog
              id="metaPlayerHelpDialog"
              header="Documentation"
              visible={helpDialogVisible}
              onHide={() => setHelpDialogVisible(false)}
              maximizable={true}
            >
              <iframe
                src={
                  "https://capytale2.ac-paris.fr/wiki/doku.php?id=notes_partagees"
                }
                style={{ width: "100%", height: "100%" }}
                title="Documentation"
              />
            </Dialog>
          </>
        )}
        {sharedNotesType === "rich" && (
          <Editor
            placeholderText={
              mode === "create"
                ? "Écrivez le contenu initial des notes partagées ici..."
                : "Les notes partagées sont vides, vous pouvez les remplir ici..."
            }
            htmlInitialContent={initialEditorState ? undefined : ""}
            initialEditorState={initialEditorState || undefined}
            jsonSizeLimit={settings.STATEMENT_MAX_SIZE}
            onChange={(editorState) => {
              dispatch(setLexicalSharedNotesState(editorState));
              if (initialStateOnChangeDone.current) {
                dispatch(setIsMPDirty(true));
              } else {
                initialStateOnChangeDone.current = true;
              }
            }}
            onJsonSizeLimitExceeded={() => {
              dispatch(setCanSaveSharedNotes(false));
              toast.current!.show({
                summary: "Erreur",
                detail: `Le contenu des notes partagées est trop volumineux. Veuillez le réduire avant de l'enregistrer.\nPeut-être avez-vous inséré une image trop volumineuse ?`,
                severity: "error",
                life: 10000,
              });
            }}
            onJsonSizeLimitMet={() => {
              dispatch(setCanSaveSharedNotes(true));
              toast.current!.show({
                summary: "Succès",
                detail: `Le contenu des notes partagées ne dépasse plus la taille limite.`,
                severity: "success",
                life: 4000,
              });
            }}
            isEditable={isEditable}
          />
        )}
      </>
    </>
  );
});

export default SharedNotesEditor;
