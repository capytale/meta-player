import { useCapytaleRichTextEditor } from "@capytale/capytale-rich-text-editor";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import {
  selectInstructions,
  selectInstructionsType,
  selectMode,
  setCanSaveInstructions,
  setInstructionsType,
  setIsMPDirty,
  setLexicalInstructionsState,
} from "../activityData/activityDataSlice";
import { forwardRef, useImperativeHandle, useMemo, useRef } from "react";
import { Toast } from "primereact/toast";
import settings from "../../settings";
import { Button } from "primereact/button";

const InstructionsEditor: React.FC = forwardRef((_props, ref) => {
  const [Editor, getState, _canSave] = useCapytaleRichTextEditor();
  const dispatch = useAppDispatch();
  const toast = useRef<Toast>(null);
  const mode = useAppSelector(selectMode);
  const isEditable = mode === "create";
  const initialInstructions = useAppSelector(selectInstructions);
  const htmlInitialContent = useMemo(() => {
    return initialInstructions?.format === "lexical"
      ? undefined
      : initialInstructions?.value === null
        ? undefined
        : (initialInstructions?.value as string) || ""; // HTML content in value
  }, [initialInstructions]);
  const initialEditorState = useMemo(() => {
    return initialInstructions?.format === "lexical"
      ? initialInstructions?.value
      : undefined;
  }, [initialInstructions]);
  const initialStateOnChangeDone = useRef<boolean>(false);
  const instructionsType = useAppSelector(selectInstructionsType);

  useImperativeHandle(ref, () => {
    return {
      save: async () => {
        const state = await getState();
        dispatch(setLexicalInstructionsState(state.json));
      },
    };
  });

  return (
    <>
      <Toast position="bottom-right" ref={toast} />
      <>
        {instructionsType === "none" && (
          <>
            <p>Pas de consigne.</p>
            {mode === "create" && (
              <Button
                label="Créer une consigne"
                onClick={() => dispatch(setInstructionsType("rich"))}
              />
            )}
          </>
        )}
        {instructionsType === "rich" && (
          <Editor
            placeholderText="Écrivez la consigne ici..."
            htmlInitialContent={htmlInitialContent}
            initialEditorState={initialEditorState}
            jsonSizeLimit={settings.STATEMENT_MAX_SIZE}
            onChange={(editorState) => {
              dispatch(setLexicalInstructionsState(editorState));
              if (initialStateOnChangeDone.current) {
                dispatch(setIsMPDirty(true));
              } else {
                initialStateOnChangeDone.current = true;
              }
            }}
            onJsonSizeLimitExceeded={() => {
              dispatch(setCanSaveInstructions(false));
              toast.current!.show({
                summary: "Erreur",
                detail: `Le contenu de la consigne est trop volumineux. Veuillez le réduire avant de l'enregistrer.\nPeut-être avez-vous inséré une image trop volumineuse ?`,
                severity: "error",
                life: 10000,
              });
            }}
            onJsonSizeLimitMet={() => {
              dispatch(setCanSaveInstructions(true));
              toast.current!.show({
                summary: "Succès",
                detail: `Le contenu de la consigne ne dépasse plus la taille limite.`,
                severity: "success",
                life: 4000,
              });
            }}
            isEditable={isEditable}
          />
        )}
      </>
    </>
  );
});

export default InstructionsEditor;
