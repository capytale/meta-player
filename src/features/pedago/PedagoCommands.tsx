import { useAppDispatch, useAppSelector } from "../../app/hooks";
import {
  selectHasGradingOrComments,
  selectInstructionsType,
  selectMode,
  selectPdfInstructions,
  selectSharedNotesType,
  setInstructionsType,
  setSharedNotesType,
} from "../activityData/activityDataSlice";
import {
  PedagoTab,
  selectIsGradingVisible,
  selectOrientation,
  selectPedagoTab,
  setPedagoTab,
  toggleIsGradingVisible,
  toggleIsPedagoVisible,
} from "../layout/layoutSlice";
import styles from "./style.module.scss";
import { Button } from "primereact/button";
import settings from "../../settings";
import { SelectButton } from "primereact/selectbutton";
import { FC, useMemo } from "react";
import { TabMenu } from "primereact/tabmenu";
import { classNames } from "primereact/utils";

export const PedagoCommands = () => {
  const isHorizontal = useAppSelector(selectOrientation) === "horizontal";
  return isHorizontal ? (
    <HorizontalPedagoCommands />
  ) : (
    <VerticalPedagoCommands />
  );
};

export const CloseOnlyPedagoCommands: FC = () => {
  const dispatch = useAppDispatch();
  return (
    <div className={styles.closeOnlyPedagoCommands}>
      <Button
        severity="secondary"
        icon="pi pi-times"
        rounded
        text
        aria-label="Masquer consignes et évaluation"
        tooltip="Masquer l'évaluation"
        tooltipOptions={{
          position: "left",
          showDelay: settings.TOOLTIP_SHOW_DELAY,
        }}
        onClick={() => dispatch(toggleIsPedagoVisible())}
        style={{ flexShrink: 0 }}
      />
    </div>
  );
};

type DocumentSelectorHzItem = {
  name: string;
  value: PedagoTab;
  tooltip: string;
};

type DocumentSelectorItem = DocumentSelectorHzItem & {
  template: (item: any) => JSX.Element;
};

const HorizontalPedagoCommands = () => {
  const mode = useAppSelector(selectMode);
  const dispatch = useAppDispatch();
  return (
    <div className={styles.pedagoCommands}>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          width: "100%",
          paddingLeft: "4px",
        }}
      >
        {(mode === "assignment" || mode === "review") && (
          <ShowHideGradingButton />
        )}
        <div></div>
        <Button
          severity="secondary"
          icon="pi pi-times"
          rounded
          text
          aria-label="Masquer consignes et évaluation"
          tooltip="Masquer les consignes"
          tooltipOptions={{
            position: "left",
            showDelay: settings.TOOLTIP_SHOW_DELAY,
          }}
          onClick={() => dispatch(toggleIsPedagoVisible())}
          style={{ flexShrink: 0 }}
        />
      </div>
      <HorizontalDocumentSelector />
    </div>
  );
};

const DocumentSelectorItemActions: FC<{
  item: DocumentSelectorHzItem;
}> = ({ item }) => {
  const mode = useAppSelector(selectMode);
  const itemTemplate = (option: any) => {
    return <i className={option.icon}></i>;
  };

  const instructionsType = useAppSelector(selectInstructionsType);
  const sharedNotesType = useAppSelector(selectSharedNotesType);
  const pdfInstructions = useAppSelector(selectPdfInstructions);
  const dispatch = useAppDispatch();

  return (
    <>
      {mode === "create" && (
        <>
          <div className={styles.pdfActions}>
            {item.value === "pdf" && !pdfInstructions && (
              <i className="pi pi-eye-slash"></i>
            )}
            {item.value === "pdf" && pdfInstructions && (
              <i className="pi pi-file-pdf"></i>
            )}
          </div>
          {item.value !== "pdf" && (
            <SelectButton
              aria-label={`Sélectionner le type de ${item.name}`}
              className={styles.smallSelectButton}
              value={
                item.value === "instructions"
                  ? instructionsType
                  : sharedNotesType
              }
              onChange={(e) => {
                if (item.value === "instructions") {
                  dispatch(setInstructionsType(e.value));
                } else {
                  dispatch(setSharedNotesType(e.value));
                }
              }}
              itemTemplate={itemTemplate}
              optionLabel="value"
              allowEmpty={false}
              options={[
                {
                  label: "Éditeur riche",
                  value: "rich",
                  icon: "pi pi-pen-to-square",
                },
                {
                  label: "Pas de consigne",
                  value: "none",
                  icon: "pi pi-eye-slash",
                },
              ]}
              tooltip={item.tooltip}
              tooltipOptions={{
                position: "left",
                showDelay: settings.TOOLTIP_SHOW_DELAY,
              }}
            />
          )}
        </>
      )}
    </>
  );
};

const HorizontalDocumentSelector = () => {
  const pedagoTab = useAppSelector(selectPedagoTab);
  const instructionsType = useAppSelector(selectInstructionsType);
  const mode = useAppSelector(selectMode);
  // const pdfInstructions = useAppSelector(selectPdfInstructions);
  const sharedNotesType = useAppSelector(selectSharedNotesType);
  const dispatch = useAppDispatch();

  const items: DocumentSelectorHzItem[] = useMemo(() => {
    const allItems: DocumentSelectorHzItem[] = [];
    if (mode === "create" || instructionsType !== "none") {
      allItems.push({
        name: "Consignes",
        value: "instructions",
        tooltip: "Activer/désactiver les consignes",
      });
    }
    /* // Removed PDF for now
    if (mode === "create" || pdfInstructions) {
      allItems.push({
        name: "PDF",
        value: "pdf",
        tooltip: "Activer/désactiver le PDF",
      });
    }
    */
    if (mode === "create" || sharedNotesType !== "none") {
      allItems.push({
        name: "Notes partagées",
        value: "sharedNotes",
        tooltip: "Activer/désactiver les notes partagées",
      });
    }
    return allItems;
  }, []);

  return (
    <div className={styles.pedagoHorizontalTabMenu}>
      {items.map((item) => (
        <div
          className={styles.pedagoHorizontalTab}
          role="button"
          aria-label={`Basculer vers l'onglet ${item.name}`}
          onClick={() => dispatch(setPedagoTab(item.value))}
          data-selected={item.value === pedagoTab}
          key={item.value}
        >
          <span>{item.name}</span>
          <DocumentSelectorItemActions item={item} />
        </div>
      ))}
    </div>
  );
};

const VerticalPedagoCommands = () => {
  const mode = useAppSelector(selectMode);
  const dispatch = useAppDispatch();
  return (
    <div className={styles.pedagoCommands}>
      <VerticalDocumentSelector />
      <div className={styles.pedagoSeparator}></div>
      {(mode === "assignment" || mode === "review") && (
        <ShowHideGradingButton />
      )}
      <Button
        severity="secondary"
        icon="pi pi-times"
        rounded
        text
        aria-label="Masquer consignes et évaluation"
        tooltip="Masquer les consignes"
        tooltipOptions={{
          position: "left",
          showDelay: settings.TOOLTIP_SHOW_DELAY,
        }}
        onClick={() => dispatch(toggleIsPedagoVisible())}
        style={{ flexShrink: 0 }}
      />
    </div>
  );
};

const VerticalDocumentSelector = () => {
  const pedagoTab = useAppSelector(selectPedagoTab);
  const instructionsType = useAppSelector(selectInstructionsType);
  const mode = useAppSelector(selectMode);
  // const pdfInstructions = useAppSelector(selectPdfInstructions);
  const sharedNotesType = useAppSelector(selectSharedNotesType);
  const dispatch = useAppDispatch();

  const itemRenderer = (item: DocumentSelectorItem) => (
    <a
      className={classNames("p-menuitem-link", styles.pedagoTab)}
      onClick={() => dispatch(setPedagoTab(item.value))}
    >
      <span>{item.name}</span>
      <DocumentSelectorItemActions item={item} />
    </a>
  );

  const items: DocumentSelectorItem[] = useMemo(() => {
    const allItems: DocumentSelectorItem[] = [];
    if (mode === "create" || instructionsType !== "none") {
      allItems.push({
        name: "Consignes",
        value: "instructions",
        tooltip: "Activer/désactiver les consignes",
        template: (item: any) => itemRenderer(item),
      });
    }
    /* // Removed PDF for now
    if (mode === "create" || pdfInstructions) {
      allItems.push({
        name: "PDF",
        value: "pdf",
        tooltip: "Activer/désactiver le PDF",
        template: (item: any) => itemRenderer(item),
      });
    }
    */
    if (mode === "create" || sharedNotesType !== "none") {
      allItems.push({
        name: "Notes partagées",
        value: "sharedNotes",
        tooltip: "Activer/désactiver les notes partagées",
        template: (item: any) => itemRenderer(item),
      });
    }
    return allItems;
  }, []);
  const activeIndex = useMemo(() => {
    const index = items.findIndex((item) => item.value === pedagoTab);
    return index;
  }, [items, pedagoTab]);

  return (
    <TabMenu
      className={styles.pedagoTabMenu}
      model={items}
      activeIndex={activeIndex}
      onTabChange={(e) => {
        dispatch(setPedagoTab(items[e.index].value));
      }}
    />
  );
};

const ShowHideGradingButton: React.FC = () => {
  const dispatch = useAppDispatch();
  const mode = useAppSelector(selectMode);
  const hasGradingOrComments = useAppSelector(selectHasGradingOrComments);
  const isGradingVisible = useAppSelector(selectIsGradingVisible);
  const canShowGrading = hasGradingOrComments || mode === "review";

  return (
    <Button
      size="small"
      icon="pi pi-graduation-cap"
      disabled={!canShowGrading}
      severity={canShowGrading ? "info" : "secondary"}
      aria-label="Afficher/Masquer la notation"
      tooltip={canShowGrading ? "Afficher/Masquer la notation" : "Pas de note"}
      tooltipOptions={{
        position: "left",
        showDelay: settings.TOOLTIP_SHOW_DELAY,
        showOnDisabled: true,
      }}
      onClick={() => dispatch(toggleIsGradingVisible())}
      style={{ flexShrink: 0 }}
      outlined={!isGradingVisible || !canShowGrading}
    />
  );
};
