.onlyGradingPedago {
  display: flex;
  width: 100%;
  :global(.layout-horizontal) & {
    flex-direction: row-reverse;
  }
  :global(.layout-vertical) & {
    flex-direction: column;
  }
}

.pedago {
  display: flex;
  height: 100%;
  width: 100%;
  :global(.layout-horizontal) & {
    flex-direction: row-reverse;
  }
  :global(.layout-vertical) & {
    flex-direction: column;
  }
}

.closeOnlyPedagoCommands {
  flex-direction: column;
  flex-shrink: 0;
  display: flex;
  background-color: var(--surface-200);
  :global(.layout-vertical) & {
    gap: 8px;
    flex-direction: row-reverse;
    align-items: center;
  }
}

.pedagoCommands {
  flex-direction: column;
  flex-shrink: 0;
  display: flex;
  background-color: var(--surface-200);
  :global(.layout-horizontal) & > *:first-child {
    background-color: var(--surface-300);
  }
  :global(.layout-vertical) & {
    gap: 8px;
    flex-direction: row;
    align-items: center;
  }
}

.pdfActions {
  padding: 10px 0;
  display: flex;
  :global(.layout-horizontal) & {
    padding-right: 9px;
  }
}

.pedagoHorizontalTabMenu {
  display: flex;
  flex-direction: column;
  overflow-y: auto;
}

.pedagoHorizontalTab {
  display: flex;
  gap: 20px;
  align-items: center;
  justify-content: space-between;
  color: var(--text-color-secondary);
  padding: 0 16px;
  transition: background-color 0.2s;
  user-select: none;
  cursor: pointer;
  &[data-selected="true"] {
    background-color: var(--surface-100);
    color: var(--text-color);
  }
  &:hover,
  &:focus {
    background-color: var(--surface-50);
  }
  & > span {
    padding: 13px 0;
  }
  & > i {
    padding-right: 8px;
  }
}

.dropzone {
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: 16px;
  cursor: pointer;
  outline: none;
}

.dropzoneRectangle {
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  flex: 1;
  padding: 20px;
  border: 2px dashed var(--surface-100);
  border-radius: 2px;
  background-color: var(--surface-50);
  color: var(--text-color-secondary);
  cursor: pointer;
  outline: none;
  transition: border-color 0.2s ease-in-out;

  &[data-is-ficused="true"] {
    border-color: var(--primary-300);
  }
  &[data-is-drag-accept="true"] {
    border-color: var(--green-300);
  }
  &[data-is-drag-reject="true"] {
    border-color: var(--red-300);
  }
}

.smallSelectButton {
  flex-shrink: 0;
  & > * {
    padding: 11px 8px;
  }
}

.pedagoSeparator {
  flex-grow: 1;
}

.pedagoSplitter {
  height: 100%;
  border: none;
  border-radius: 0;
  background-color: var(--surface-0);
  & > :global(.p-splitter-panel) {
    overflow-y: hidden;
  }
}

.gradingPanel {
  display: flex;
  gap: 4px;
  width: 100%;
}

*:has(> .fullSizePanel) {
  padding: 4px;
}

.fullSizePanel {
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  & > :global(.p-panel-header) {
    flex-shrink: 0;
  }
  & > :global(.p-toggleable-content) {
    flex-grow: 1;
    overflow: hidden;
  }
  & > :global(.p-toggleable-content) > * {
    height: 100%;
    padding: 0;
    overflow: auto;
    display: flex;
  }
}

.pedagoTabMenu {
  align-self: stretch;
  & > * {
    background-color: transparent;
    flex-wrap: wrap;
    min-height: 100%;
  }
}

.pedagoTab {
  background-color: transparent;
  padding: 4px 1.25rem;
  gap: 16px;
  font-weight: normal;
  border-right: 1px solid var(--surface-300);
  border-radius: 0;
  :global(.p-highlight) & {
    z-index: 1;
  }
}

.pedagoContent {
  flex-grow: 1;
  overflow-y: auto;
  text-align: center;
}

.pedagoFeedbackPanel {
  flex-grow: 3;
  flex-basis: 0;
}

.pedagoGradePanel {
  flex-grow: 2;
  flex-basis: 0;
}

.fullTextarea {
  width: 100%;
  padding: 0.7em;
  border: 1px solid transparent;
  resize: none;
  height: 100%;
  background-color: transparent;
  font-family: Arial, Helvetica, sans-serif;
}

*:has(> .fullTextarea) {
  height: 100%;
  width: 100%;
  overflow: hidden;
}
