import { FC, useEffect, useMemo } from "react";
import { useDropzone } from "react-dropzone";
import styles from "./style.module.scss";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import {
  selectMode,
  selectPdfInstructions,
  setPdfInstructions,
} from "../activityData/activityDataSlice";
import { Button } from "primereact/button";

export const PdfEditor: FC = () => {
  const dispatch = useAppDispatch();
  const {
    getRootProps,
    getInputProps,
    isFocused,
    isDragAccept,
    isDragReject,
    acceptedFiles,
  } = useDropzone({
    accept: {
      "application/pdf": [".pdf"],
    },
  });
  useEffect(() => {
    if (acceptedFiles.length) {
      dispatch(setPdfInstructions(acceptedFiles[0]));
    }
  }, [acceptedFiles]);
  const mode = useAppSelector(selectMode);
  const pdfInstructions = useAppSelector(selectPdfInstructions);
  const pdfData = useMemo(
    () => (pdfInstructions ? URL.createObjectURL(pdfInstructions) : null),
    [pdfInstructions],
  );
  return (
    <>
      {pdfData && (
        <div
          style={{
            width: "100%",
            height: "100%",
            position: "relative",
            overflow: "hidden",
          }}
        >
          <object
            data={pdfData + "#?navpanes=0"}
            type="application/pdf"
            width="100%"
            height="100%"
          >
            <p>
              Votre navigateur ne supporte pas les PDFs, vous pouvez{" "}
              <a href={pdfData}>télécharger le fichier</a> à la place.
            </p>
          </object>
          <div className="meta-player-content-cover"></div>
          {mode === "create" && (
            <Button
              label="Supprimer le PDF"
              severity="danger"
              icon="pi pi-trash"
              className={styles.deleteButton}
              onClick={() => dispatch(setPdfInstructions(null))}
              style={{ position: "absolute", bottom: "20px", right: "20px" }}
            />
          )}
        </div>
      )}
      {!pdfData && (
        <div className={styles.dropzone} {...getRootProps()}>
          <div
            className={styles.dropzoneRectangle}
            data-is-focused={isFocused}
            data-is-drag-accept={isDragAccept}
            data-is-drag-reject={isDragReject}
          >
            <input {...getInputProps()} />
            <p>Déposez un PDF ici ou cliquez pour sélectionner un fichier...</p>
            <i
              className="pi pi-file-arrow-up"
              style={{ fontSize: "2.5rem" }}
            ></i>
          </div>
        </div>
      )}
    </>
  );
};
