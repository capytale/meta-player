import { FC, useEffect } from "react";
import {
  SaveCallback,
  addAfterSave,
  removeAfterSave,
} from "./saverSlice";
import { useAppDispatch } from "../../app/hooks";

type AfterSaveActionProps = SaveCallback;

const AfterSaveAction: FC<AfterSaveActionProps> = (props) => {
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(addAfterSave(props));
    return () => {
      // Remove the callback when the component is unmounted
      dispatch(removeAfterSave(props.name));
    };
  }, [props]);
  return null;
};

export default AfterSaveAction;
