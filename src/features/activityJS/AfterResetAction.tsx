import { FC, useEffect } from "react";
import {
  SaveCallback,
  addAfterReset,
  removeAfterReset,
} from "./saverSlice";
import { useAppDispatch } from "../../app/hooks";

type AfterResetActionProps = SaveCallback;

const AfterResetAction: FC<AfterResetActionProps> = (props) => {
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(addAfterReset(props));
    return () => {
      // Remove the callback when the component is unmounted
      dispatch(removeAfterReset(props.name));
    };
  }, [props]);
  return null;
};

export default AfterResetAction;
