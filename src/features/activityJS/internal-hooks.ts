import { useEffect, useState, useRef } from "react";

import { autoLoad } from "@capytale/activity.js";

import type ActivitySession from "@capytale/activity.js/activity/activitySession/uni";

import type { ActivityBunchOptions as LoadOptions } from "@capytale/activity.js/activity/activityBunch/uni/backend";

export type { LoadOptions };

export type ActivitySessionLoaderReturnValue = {
  state: "loading" | "loaded" | "error";
  activitySession: ActivitySession | null;
  error?: any;
};

/**
 * Un hook pour utiliser activity.js
 * Charge une activité de façon réactive.
 *
 * @param id id de l'activité à charger
 * @param loadOptions les options de chargement
 * @param callback une callback appelée avec l'activité chargée
 * @returns un objet contenant l'état de chargement de l'activité et l'activité dès qu'elle est chargée
 */

export function useActivitySessionLoader(
  id: number,
  loadOptions?: LoadOptions,
  callback?: (activitySession: ActivitySession) => void,
): ActivitySessionLoaderReturnValue {
  const callbackW = useHandlerWrapper(callback);
  const [state, setState] = useState<ActivitySessionLoaderReturnValue>({
    state: "loading",
    activitySession: null,
  });
  useEffect(() => {
    let cancelled = false;
    autoLoad(loadOptions)
      .then((data) => {
        if (cancelled) return;
        callbackW(data);
        setState({
          state: "loaded",
          activitySession: data,
        });
        (window as any).capy = data;
      })
      .catch((error) => {
        if (cancelled) return;
        setState({
          state: "error",
          activitySession: null,
          error,
        });
      });
    return () => {
      cancelled = true;
      setState({
        state: "loading",
        activitySession: null,
      });
    };
  }, [id, loadOptions?.binaryDataType, loadOptions?.readOnly]);
  return state;
}

/**
 * Si un handler est passé en props à un composant, il peut changer. Il n'y a pas trop de raison mais
 * rien ne l'interdit.
 * Si un useEffect invoque ce handler, il faudrait que le handler figure dans les dépendances du useEffect.
 * Du coup, si le handler change, le useEffect est ré-exécuté.
 * Ce hook crée un wrapper immuable autour du handler passé en props. Ce wrapper ne change jamais et n'a donc
 * pas besoin de figurer dans les dépendances du useEffect.
 *
 * @param handler
 */
export function useHandlerWrapper<H extends () => any>(
  handler?: H,
): () => ReturnType<H> | void;
export function useHandlerWrapper<H extends (...args: any[]) => any>(
  handler?: H,
): (...p: Parameters<H>) => ReturnType<H> | void;
export function useHandlerWrapper<H extends (...args: any[]) => any>(
  handler?: H,
): (...p: Parameters<H>) => ReturnType<H> | void {
  const handlerRef = useRef<H>();
  handlerRef.current = handler;
  return useRef((...p: Parameters<H>) => {
    if (null == handlerRef.current) return;
    return handlerRef.current(...p);
  }).current;
}
