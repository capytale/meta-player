import { useAppDispatch, useAppSelector } from "../../app/hooks";
import {
  selectCanReset,
  selectMode,
  selectWorkflow,
  setSaveState,
} from "../activityData/activityDataSlice";
import { useActivityJS } from "./ActivityJSProvider";
import { selectBeforeReset, selectAfterReset } from "./saverSlice";

type UseResetProps = {
  onError: (errorMessage: string) => void;
};

export const useReset = (props: UseResetProps) => {
  const dispatch = useAppDispatch();
  const canReset = useAppSelector(selectCanReset);
  const beforeReset = useAppSelector(selectBeforeReset);
  const afterReset = useAppSelector(selectAfterReset);
  const activityJs = useActivityJS();
  const mode = useAppSelector(selectMode);
  const workflow = useAppSelector(selectWorkflow);
  if (mode !== "assignment" || workflow !== "current") {
    return null;
  }
  if (!canReset) {
    return null;
  }
  return async (reload: boolean = true) => {
    if (!activityJs.activitySession) {
      throw new Error("No activity session to reset");
    }
    for (const callback of Object.values(beforeReset)) {
      try {
        const v = callback();
        if (v instanceof Promise) {
          await v;
        }
      } catch (e) {
        console.error("Error in beforeReset callback", e);
        props.onError((e as any).toString());
        return;
      }
    }
    dispatch(setSaveState("saving"));
    try {
      const ab = activityJs.activitySession.activityBunch;
      // @ts-expect-error
      if (ab.assignmentNode!.content.value != null) {
        // @ts-expect-error
        ab.assignmentNode.content.value = null;
      }
      // @ts-expect-error
      if (ab.assignmentNode!.binaryData?.value != null) {
        // @ts-expect-error
        ab.assignmentNode.binaryData.value = null;
      }
      try {
        await ab.save();

        for (const callback of Object.values(afterReset)) {
          try {
            const v = callback();
            if (v instanceof Promise) {
              await v;
            }
          } catch (e) {
            console.error("Error in afterReset callback", e);
            props.onError((e as any).toString());
          }
        }
        if (reload) {
          location.reload();
        }
      } catch (e) {
        console.error("Error in reset", e);
        props.onError((e as any).toString());
        return;
      }
    } catch (e) {
      console.error("Error in reset process", e);
      props.onError((e as any).toString());
    }
    dispatch(setSaveState("idle"));
  };
};
