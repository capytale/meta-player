import {
  FC,
  PropsWithChildren,
  ReactNode,
  createContext,
  useContext,
  useEffect,
  useState,
} from "react";

import {
  ActivitySessionLoaderReturnValue,
  LoadOptions,
  useActivitySessionLoader,
} from "./internal-hooks";
import { getIdFromUrl } from "../../utils/activity";
import { useAppDispatch } from "../../app/hooks";
import ActivitySession from "@capytale/activity.js/activity/activitySession";
import { setActivityJSData } from "../activityData/activityDataSlice";

import tracker from "@capytale/activity.js/backend/capytale/tracker";

const ActivityJSContext = createContext<ActivitySessionLoaderReturnValue>({
  state: "loading",
  activitySession: null,
});

type ActivityJSProviderProps = PropsWithChildren<{
  options?: LoadOptions;
  loadingFallback?: ReactNode;
  errorFallback?:
    | keyof JSX.IntrinsicElements
    | React.JSXElementConstructor<{ children: ReactNode }>;
}>;

export const ActivityJSProvider: FC<ActivityJSProviderProps> = (props) => {
  const [loaded, setLoaded] = useState(false);
  const activityId = getIdFromUrl();
  const dispatch = useAppDispatch();

  useEffect(() => {
    tracker.trackPageView();
  }, []);

  const callback = (data: ActivitySession) => {
    //@ts-expect-error
    window.capy = data;

    const ab = data.activityBunch;

    const sharedNotesContent = ab.shared_notes.value;

    dispatch(
      setActivityJSData({
        title: ab.title.value || "",
        mode: data.mode,
        returnUrl: data.returnUrl,
        helpUrl: data.type.helpUrl,
        nid: ab.mainNode.nid,
        activityNid: ab.activityNode.nid,
        code: data.code,
        codeLink: data.codeLink,
        accessTrMode: ab.access_tr_mode.value || "",
        accessTimerange: ab.access_timerange.value,
        studentInfo: data.student
          ? {
              firstName: data.student.prenom || "",
              lastName: data.student.nom || "",
              class: data.student.classe || "",
            }
          : null,
        instructions: ab.instructions.value.lexical
          ? {
              value: ab.instructions.value.lexical,
              htmlValue: ab.instructions.value.html,
              format: "lexical",
            }
          : {
              value: null,
              htmlValue: ab.instructions.value.html || "",
              format: "html",
            },
        instructionsType:
          ab.instructions.value.lexical ||
          ab.instructions.value.html ||
          (data.mode === "create" && ab.activityNode.isNew)
            ? "rich"
            : "none",
        pdfInstructions: null,
        sharedNotesContent: sharedNotesContent,
        sharedNotesType: sharedNotesContent == null ? "none" : "rich",
        icon: data.icon || null,
        friendlyType: data.friendlyType,
        comments: ab.assignmentNode?.comments.value || null,
        grading: ab.assignmentNode?.grading.value || null,
        workflow: ab.assignmentNode?.workflow,
        antiCheat: ab.hasAntiCheat
          ? {
              passwordHash: ab.antiCheatPasswd,
              startLocked: !ab.assignmentNode?.isNew,
            }
          : null,
        hasEvaluations: ab.activityNode.has_evaluation.value,
      }),
    );
    tracker.trackActivity(data);
    setLoaded(true);
  };

  if (!activityId) {
    throw new Error("No activity id found in the URL");
  }
  const value = useActivitySessionLoader(activityId, props.options, callback);
  if (!loaded) {
    if (value.state === "loading") {
      return <>{props.loadingFallback}</>;
    } else if (value.state === "error" && props.errorFallback) {
      console.error(value.error);
      return (
        <>{<props.errorFallback>{String(value.error)}</props.errorFallback>}</>
      );
    }
  }
  return (
    <ActivityJSContext.Provider value={value}>
      {props.children}
    </ActivityJSContext.Provider>
  );
};

export const useActivityJS = () => {
  return useContext(ActivityJSContext);
};

export const useActivityJsEssentials = () => {
  const activityJs = useActivityJS();
  if (!activityJs.activitySession) {
    throw new Error("ActivityJS data not loaded");
  }
  const mode = activityJs.activitySession.mode;
  const ab = activityJs.activitySession.activityBunch;
  const hasAssignment = !!ab.assignmentNode;
  const getActivityContent = () => {
    //@ts-expect-error
    return ab.activityNode.content.value as string | null;
  };
  const getAssignmentContent = () => {
    if (!ab.assignmentNode) {
      throw new Error("No assignment node");
    }
    //@ts-expect-error
    return ab.assignmentNode.content.value as string | null;
  };
  const getContent = () => {
    if (!hasAssignment) {
      return getActivityContent();
    }
    const assignmentContent = getAssignmentContent();
    if (assignmentContent != null) {
      return assignmentContent;
    }
    return getActivityContent();
  };
  const getActivityBinaryData = () => {
    //@ts-expect-error
    return ab.activityNode.binaryData.value as any;
  };
  const getAssignmentBinaryData = () => {
    if (!ab.assignmentNode) {
      throw new Error("No assignment node");
    }
    //@ts-expect-error
    return ab.assignmentNode.binaryData.value as any;
  };
  const getBinaryData = () => {
    if (!hasAssignment) {
      return getActivityBinaryData();
    }
    const assignmentBinaryData = getAssignmentBinaryData();
    if (assignmentBinaryData != null) {
      return assignmentBinaryData;
    }
    return getActivityBinaryData();
  };
  const setActivityContent = (content: string | null) => {
    //@ts-expect-error
    ab.activityNode.content.value = content;
  };
  const setAssignmentContent = (content: string | null) => {
    if (!ab.assignmentNode) {
      throw new Error("No assignment node");
    }
    //@ts-expect-error
    ab.assignmentNode.content.value = content;
  };
  const setContent = (content: string | null) => {
    if (!hasAssignment) {
      setActivityContent(content);
    } else {
      setAssignmentContent(content);
    }
  };
  const setActivityBinaryData = (data: any | null) => {
    //@ts-expect-error
    ab.activityNode.binaryData.value = data;
  };
  const setAssignmentBinaryData = (data: any | null) => {
    if (!ab.assignmentNode) {
      throw new Error("No assignment node");
    }
    //@ts-expect-error
    ab.assignmentNode.binaryData.value = data;
  };
  const setBinaryData = (data: any | null) => {
    if (!hasAssignment) {
      setActivityBinaryData(data);
    } else {
      setAssignmentBinaryData(data);
    }
  };
  const title = ab.title.value;
  const nid = ab.mainNode.nid;
  return {
    mode,
    title,
    hasAssignment,
    nid,
    getActivityContent,
    getAssignmentContent,
    getContent,
    getActivityBinaryData,
    getAssignmentBinaryData,
    getBinaryData,
    setActivityContent,
    setAssignmentContent,
    setContent,
    setActivityBinaryData,
    setAssignmentBinaryData,
    setBinaryData,
  };
};
