import { FC, useEffect } from "react";
import {
  SaveCallback,
  addBeforeSave,
  removeBeforeSave,
} from "./saverSlice";
import { useAppDispatch } from "../../app/hooks";

type BeforeSaveActionProps = SaveCallback;

const BeforeSaveAction: FC<BeforeSaveActionProps> = (props) => {
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(addBeforeSave(props));
    return () => {
      // Remove the callback when the component is unmounted
      dispatch(removeBeforeSave(props.name));
    };
  }, [props]);
  return null;
};

export default BeforeSaveAction;
