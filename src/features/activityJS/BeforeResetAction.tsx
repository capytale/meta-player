import { FC, useEffect } from "react";
import {
  SaveCallback,
  addBeforeReset,
  removeBeforeReset,
} from "./saverSlice";
import { useAppDispatch } from "../../app/hooks";

type BeforeResetActionProps = SaveCallback;

const BeforeResetAction: FC<BeforeResetActionProps> = (props) => {
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(addBeforeReset(props));
    return () => {
      // Remove the callback when the component is unmounted
      dispatch(removeBeforeReset(props.name));
    };
  }, [props]);
  return null;
};

export default BeforeResetAction;
