import { FC, useEffect, useRef } from "react";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import {
  selectSharedNotesContent,
  selectComments,
  selectGrading,
  selectInstructions,
  selectSaveState,
  setIsMPDirty,
  setIsPlayerDirty,
  setSaveState,
  setWorkflow,
  selectSharedNotesType,
  selectInstructionsType,
} from "../activityData/activityDataSlice";
import { useActivityJS } from "./ActivityJSProvider";
import { Toast } from "primereact/toast";
import { selectBeforeSave, selectAfterSave } from "./saverSlice";

const Saver: FC<{}> = () => {
  const dispatch = useAppDispatch();
  const activityJs = useActivityJS();
  const instructions = useAppSelector(selectInstructions);
  const instructionsType = useAppSelector(selectInstructionsType);
  const sharedNotesContent = useAppSelector(selectSharedNotesContent);
  const sharedNotesType = useAppSelector(selectSharedNotesType);
  const comments = useAppSelector(selectComments);
  const grading = useAppSelector(selectGrading);
  const beforeSave = useAppSelector(selectBeforeSave);
  const afterSave = useAppSelector(selectAfterSave);

  const toast = useRef<Toast>(null);

  const executePrepareSave = () => {};
  const save = async () => {
    if (!activityJs.activitySession) {
      throw new Error("No activity session to save");
    }
    for (const callback of Object.values(beforeSave)) {
      try {
        const v = callback();
        if (v instanceof Promise) {
          await v;
        }
      } catch (e) {
        console.error("Error in beforeSave callback", e);
        dispatch(setSaveState("idle"));
        toast.current!.show({
          summary: "Erreur : enregistrement annulé",
          detail: (e as any).toString(),
          severity: "error",
          life: 10000,
        });
        return;
      }
    }
    const ab = activityJs.activitySession.activityBunch;
    if (activityJs.activitySession.mode === "create") {
      if (instructionsType === "rich") {
        ab.instructions.value = {
          lexical:
            instructions?.format === "lexical"
              ? typeof instructions.value === "string"
                ? instructions.value
                : JSON.stringify(instructions.value)
              : null,
          html:
            instructions?.format === "lexical"
              ? instructions.htmlValue
              : (instructions?.value as string),
        };
      } else {
        ab.instructions.value = {
          lexical: null,
          html: null,
        };
      }
    }
    if (activityJs.activitySession.mode === "review") {
      ab.assignmentNode!.comments.value = comments || "";
      ab.assignmentNode!.grading.value = grading || "";
    }
    if (
      activityJs.activitySession.mode === "create" ||
      activityJs.activitySession.mode === "review" ||
      activityJs.activitySession.mode === "assignment"
    ) {
      if (sharedNotesContent && sharedNotesType === "rich") {
        ab.shared_notes.value =
          typeof sharedNotesContent === "string"
            ? sharedNotesContent
            : JSON.stringify(sharedNotesContent);
      } else {
        ab.shared_notes.value = null;
      }
    }
    try {
      await ab.save();
      dispatch(setSaveState("idle"));
      if (
        activityJs.activitySession.mode === "assignment" ||
        activityJs.activitySession.mode === "review"
      ) {
        dispatch(setWorkflow(ab.assignmentNode!.workflow));
      }
      dispatch(setIsPlayerDirty(false));
      dispatch(setIsMPDirty(false));
      toast.current!.show({
        summary: "Sauvegarde réussie",
        detail: "L'activité a bien été enregistrée.",
        severity: "success",
        life: 2000,
      });
      for (const callback of Object.values(afterSave)) {
        try {
          const v = callback();
          if (v instanceof Promise) {
            await v;
          }
        } catch (e) {
          console.error("Error in afterSave callback", e);
          dispatch(setSaveState("idle"));
          toast.current!.show({
            summary: "Erreur après sauvegarde",
            detail: (e as any).toString(),
            severity: "error",
            life: 10000,
          });
        }
      }
    } catch (error: any) {
      console.error("Error saving", error);
      dispatch(setSaveState("idle"));
      toast.current!.show({
        summary: "Erreur lors de l'enregistrement",
        detail: error.toString(),
        severity: "error",
        life: 10000,
      });
    }
  };

  const saveState = useAppSelector(selectSaveState);
  useEffect(() => {
    if (saveState === "should-save") {
      dispatch(setSaveState("saving"));
      executePrepareSave();
      save();
    }
  }, [saveState]);
  return (
    <>
      <Toast position="bottom-right" ref={toast} />
    </>
  );
};

export default Saver;
