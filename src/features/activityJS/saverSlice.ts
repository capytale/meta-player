import type { PayloadAction } from "@reduxjs/toolkit";
import { createAppSlice } from "../../app/createAppSlice";

type CallbackType = () => void | Promise<void>;

export type SaveCallback = {
  name: string;
  callback: CallbackType;
};

export interface SaverState {
  beforeSave: { [key: string]: CallbackType };
  afterSave: { [key: string]: CallbackType };
  beforeReset: { [key: string]: CallbackType };
  afterReset: { [key: string]: CallbackType };
}

const initialState: SaverState = {
  beforeSave: {},
  afterSave: {},
  beforeReset: {},
  afterReset: {},
};

// If you are not using async thunks you can use the standalone `createSlice`.
export const saverSlice = createAppSlice({
  name: "saver",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: (create) => ({
    // Use the `PayloadAction` type to declare the contents of `action.payload`
    addBeforeSave: create.reducer(
      (state, action: PayloadAction<SaveCallback>) => {
        state.beforeSave[action.payload.name] = action.payload.callback;
      },
    ),
    addAfterSave: create.reducer(
      (state, action: PayloadAction<SaveCallback>) => {
        state.afterSave[action.payload.name] = action.payload.callback;
      },
    ),
    addBeforeReset: create.reducer(
      (state, action: PayloadAction<SaveCallback>) => {
        state.beforeReset[action.payload.name] = action.payload.callback;
      },
    ),
    addAfterReset: create.reducer(
      (state, action: PayloadAction<SaveCallback>) => {
        state.afterReset[action.payload.name] = action.payload.callback;
      },
    ),
    removeBeforeSave: create.reducer((state, action: PayloadAction<string>) => {
      delete state.beforeSave[action.payload];
    }),
    removeAfterSave: create.reducer((state, action: PayloadAction<string>) => {
      delete state.afterSave[action.payload];
    }),
    removeBeforeReset: create.reducer(
      (state, action: PayloadAction<string>) => {
        delete state.beforeReset[action.payload];
      },
    ),
    removeAfterReset: create.reducer((state, action: PayloadAction<string>) => {
      delete state.afterReset[action.payload];
    }),
  }),
  // You can define your selectors here. These selectors receive the slice
  // state as their first argument.
  selectors: {
    selectBeforeSave: (saver) => saver.beforeSave,
    selectAfterSave: (saver) => saver.afterSave,
    selectBeforeReset: (saver) => saver.beforeReset,
    selectAfterReset: (saver) => saver.afterReset,
  },
});

// Action creators are generated for each case reducer function.
export const {
  addBeforeSave,
  removeBeforeSave,
  addAfterSave,
  removeAfterSave,
  addBeforeReset,
  removeBeforeReset,
  addAfterReset,
  removeAfterReset,
} = saverSlice.actions;

// Selectors returned by `slice.selectors` take the root state as their first argument.
export const {
  selectBeforeSave,
  selectAfterSave,
  selectBeforeReset,
  selectAfterReset,
} = saverSlice.selectors;
