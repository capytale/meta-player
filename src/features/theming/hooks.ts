import { useAppSelector } from "../../app/hooks"
import { selectThemeIsDark } from "./themingSlice";

export const useThemeType = () => {
  const themeIsDark = useAppSelector(selectThemeIsDark);
  return themeIsDark ? "dark" : "light";
}