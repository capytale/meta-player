import { FC, useCallback, useContext, useEffect, useMemo } from "react";
import { PrimeReactContext } from "primereact/api";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import {
  selectAutoSwitch,
  selectPreviousThemePath,
  selectThemePath,
  switchToTheme,
} from "./themingSlice";

const ThemeSwitcher: FC = () => {
  const dispatch = useAppDispatch();
  const mediaMatch = useMemo(
    () => window.matchMedia("(prefers-color-scheme: dark)"),
    [],
  );
  const { changeTheme } = useContext(PrimeReactContext);
  const themePath = useAppSelector(selectThemePath);
  const previousThemePath = useAppSelector(selectPreviousThemePath);
  const autoSwitch = useAppSelector(selectAutoSwitch);
  const handlePrefersColorScheme = useCallback((e: MediaQueryListEvent) => {
    dispatch(switchToTheme(e.matches ? "dark" : "light"));
  }, []);
  useEffect(() => {
    if (themePath !== previousThemePath) {
      changeTheme &&
        changeTheme(
          previousThemePath as string,
          themePath as string,
          "theme-link",
        );
    }
  }, [themePath]);
  useEffect(() => {
    if (autoSwitch) {
      if (mediaMatch.matches) {
        dispatch(switchToTheme("dark"));
      } else {
        dispatch(switchToTheme("light"));
      }
      mediaMatch.addEventListener("change", handlePrefersColorScheme);
      return () => {
        mediaMatch.removeEventListener("change", handlePrefersColorScheme);
      };
    }
  }, [autoSwitch]);

  return <></>;
};

export default ThemeSwitcher;
