import type { PayloadAction } from "@reduxjs/toolkit";
import { createAppSlice } from "../../app/createAppSlice";

type ThemeData = {
  id: string;
  name: string;
  path: string;
  isDark: boolean;
};

export interface ThemingState {
  themes: ThemeData[];
  currentTheme: number;
  previousTheme: number;
  autoSwitch: boolean;
}

const initialState: ThemingState = {
  themes: [
    {
      id: "light",
      name: "Clair",
      path: "https://cdn.ac-paris.fr/capytale/meta-player/themes/lara-light-blue/theme.css",
      isDark: false,
    },
    {
      id: "dark",
      name: "Sombre",
      path: "https://cdn.ac-paris.fr/capytale/meta-player/themes/lara-dark-blue/theme.css",
      isDark: true,
    },
  ],
  currentTheme: 0,
  previousTheme: 0,
  autoSwitch: false,
};

// If you are not using async thunks you can use the standalone `createSlice`.
export const themingSlice = createAppSlice({
  name: "theming",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: (create) => ({
    switchToNextTheme: create.reducer((state) => {
      // Redux Toolkit allows us to write "mutating" logic in reducers. It
      // doesn't actually mutate the state because it uses the Immer library,
      // which detects changes to a "draft state" and produces a brand new
      // immutable state based off those changes
      state.previousTheme = state.currentTheme;
      state.currentTheme = (state.currentTheme + 1) % state.themes.length;
    }),
    // Use the `PayloadAction` type to declare the contents of `action.payload`
    switchToTheme: create.reducer((state, action: PayloadAction<string>) => {
      state.previousTheme = state.currentTheme;
      state.currentTheme = state.themes.findIndex(
        (theme) => theme.id === action.payload,
      );
    }),
    setAutoSwitch: create.reducer((state, action: PayloadAction<boolean>) => {
      state.autoSwitch = action.payload;
    }),
  }),
  // You can define your selectors here. These selectors receive the slice
  // state as their first argument.
  selectors: {
    selectThemePath: (theming) => theming.themes[theming.currentTheme].path,
    selectPreviousThemePath: (theming) =>
      theming.themes[theming.previousTheme].path,
    selectThemeName: (theming) => theming.themes[theming.currentTheme].name,
    selectThemeIsDark: (theming) => theming.themes[theming.currentTheme].isDark,
    selectAutoSwitch: (theming) => theming.autoSwitch,
    selectThemeNameOrAuto: (theming) => {
      return theming.autoSwitch
        ? "Système (auto)"
        : theming.themes[theming.currentTheme].name;
    },
  },
});

// Action creators are generated for each case reducer function.
export const { switchToNextTheme, switchToTheme, setAutoSwitch } =
  themingSlice.actions;

// Selectors returned by `slice.selectors` take the root state as their first argument.
export const {
  selectThemePath,
  selectPreviousThemePath,
  selectThemeName,
  selectThemeIsDark,
  selectAutoSwitch,
  selectThemeNameOrAuto,
} = themingSlice.selectors;
