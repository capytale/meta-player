import { useAppSelector } from "../../app/hooks";
import { selectOrientation } from "./layoutSlice";

export const useOrientation = () => {
  const orientation = useAppSelector(selectOrientation);
  return orientation;
};
