import type { PayloadAction } from "@reduxjs/toolkit";
import { createAppSlice } from "../../app/createAppSlice";

export type Orientation = "horizontal" | "vertical";

export type PedagoTab = "instructions" | "sharedNotes" | "pdf";

export interface LayoutState {
  orientation: Orientation;
  isPedagoVisible: boolean;
  isGradingVisible: boolean;
  pedagoTab: PedagoTab;
  showWorkflow: boolean;
  showSaveForStudents: boolean;
}

export const initialState: LayoutState = {
  orientation: "horizontal",
  isPedagoVisible: true,
  isGradingVisible: true,
  pedagoTab: "instructions",
  showWorkflow: true,
  showSaveForStudents: true,
};

// If you are not using async thunks you can use the standalone `createSlice`.
export const layoutSlice = createAppSlice({
  name: "layout",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: (create) => ({
    toggleLayout: create.reducer((state) => {
      // Redux Toolkit allows us to write "mutating" logic in reducers. It
      // doesn't actually mutate the state because it uses the Immer library,
      // which detects changes to a "draft state" and produces a brand new
      // immutable state based off those changes
      state.orientation =
        state.orientation === "horizontal" ? "vertical" : "horizontal";
    }),
    // Use the `PayloadAction` type to declare the contents of `action.payload`
    setLayout: create.reducer((state, action: PayloadAction<Orientation>) => {
      state.orientation = action.payload;
    }),
    toggleIsPedagoVisible: create.reducer((state) => {
      state.isPedagoVisible = !state.isPedagoVisible;
    }),
    setIsGradingVisible: create.reducer(
      (state, action: PayloadAction<boolean>) => {
        state.isGradingVisible = action.payload;
      },
    ),
    toggleIsGradingVisible: create.reducer((state) => {
      state.isGradingVisible = !state.isGradingVisible;
    }),
    setPedagoTab: create.reducer((state, action: PayloadAction<PedagoTab>) => {
      state.pedagoTab = action.payload;
    }),
  }),
  // You can define your selectors here. These selectors receive the slice
  // state as their first argument.
  selectors: {
    selectOrientation: (layout) => layout.orientation,
    selectIsPedagoVisible: (layout) => layout.isPedagoVisible,
    selectIsGradingVisible: (layout) => layout.isGradingVisible,
    selectPedagoTab: (layout) => layout.pedagoTab,
    selectShowWorkflow: (layout) => layout.showWorkflow,
    selectShowSaveForStudents: (layout) => layout.showSaveForStudents,
  },
});

// Action creators are generated for each case reducer function.
export const {
  toggleLayout,
  setLayout,
  toggleIsPedagoVisible,
  setIsGradingVisible,
  toggleIsGradingVisible,
  setPedagoTab,
} = layoutSlice.actions;

// Selectors returned by `slice.selectors` take the root state as their first argument.
export const {
  selectOrientation,
  selectIsPedagoVisible,
  selectIsGradingVisible,
  selectPedagoTab,
  selectShowWorkflow,
  selectShowSaveForStudents,
} = layoutSlice.selectors;
