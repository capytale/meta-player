import { useMemo } from "react";
import { useAppDispatch, useAppSelector } from "../../app/hooks"
import { useActivityJS } from "../activityJS/ActivityJSProvider";
import { AttachedFileData, refreshAttachedFiles, selectAttachedFilesOptions, selectAttachedFilesRefresher, setPreviewFile } from "./functionalitiesSlice"

export const useAttachedFiles = () => {
  const attachedFilesOptions = useAppSelector(selectAttachedFilesOptions);
  const attachedFilesRefresher = useAppSelector(selectAttachedFilesRefresher);

  const activityJS = useActivityJS();
  const files = useMemo(
    () =>
      activityJS.activitySession?.activityBunch.activityNode.attached_files
        .items || [],
    [activityJS, attachedFilesRefresher],
  );
  const filesData = useMemo<AttachedFileData[]>(() => {
    return files.map((file) => {
      const name = decodeURIComponent(file.split("/").pop() || "");
      return {
        name: name,
        urlOrId: file,
        isTemporary: false,
        downloadMode: "default",
        copyLinkMode: "default",
        interactMode: "preview",
      };
    });
  }, [files]);

  const treatedFilesData = useMemo(() => {
    if (!attachedFilesOptions.enabled || !attachedFilesOptions.listFilesMiddleware) {
      return filesData;
    }
    return attachedFilesOptions.listFilesMiddleware(filesData);
  }, [filesData, attachedFilesOptions, attachedFilesRefresher]);

  return treatedFilesData;
};

export const useRefreshAttachedFiles = () => {
  const dispatch = useAppDispatch();
  return () => dispatch(refreshAttachedFiles());
};

export const usePreviewTextFile = () => {
  const dispatch = useAppDispatch();
  return (title: string, content: string) => {
    dispatch(
      setPreviewFile({
        title,
        type: "text",
        content,
      }),
    );
  }
};

export const usePreviewImageFile = () => {
  const dispatch = useAppDispatch();
  return (title: string, url: string) => {
    dispatch(
      setPreviewFile({
        title,
        type: "image",
        url,
      }),
    );
  }
};
