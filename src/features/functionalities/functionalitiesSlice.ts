import type { PayloadAction } from "@reduxjs/toolkit";
import { createAppSlice } from "../../app/createAppSlice";

export type AttachedFileData = {
  name: string;
  isTemporary: boolean;
  downloadMode: "default" | "custom" | "none";
  copyLinkMode: "default" | "custom" | "none";
  interactMode: "download" | "preview" | "custom" | "none";
  urlOrId: string;
}

export type UploadedFileInfo = {
  source: URL,
  name: string,
  size: number,
  file: File,
};

export type AttachedFilesOptions = {
  enabled: boolean;
  uploadTemporaryFiles?: {
    uploadButtonLabel?: string;
    mimeTypes?: string[];
    multiple: boolean;
    handler: (fileInfos: UploadedFileInfo[]) => any;
  };
  listFilesMiddleware?: (originalFiles: AttachedFileData[]) => AttachedFileData[];
  customHandlers?: {
    downloadFile?: ((originalFile: AttachedFileData) => any);
    copyFileLink?: ((originalFile: AttachedFileData) => any);
    interactWithFile?: ((originalFile: AttachedFileData) => any);
  };
  onViewFiles?: () => any;
};

export type PreviewFile = { title: string } & (
  { type: "text", content: string } |
  { type: "image", url: string }
);

export type FunctionalitiesState = {
  attachedFilesOptions: AttachedFilesOptions;
  attachedFilesRefresher: number;
  previewFile: PreviewFile | null;
}

export const defaultAttachedFilesOptions: AttachedFilesOptions = {
  enabled: false,
};

export const initialState: FunctionalitiesState = {
  attachedFilesOptions: defaultAttachedFilesOptions,
  attachedFilesRefresher: 0,
  previewFile: null,
};

// If you are not using async thunks you can use the standalone `createSlice`.
export const functionalitiesSlice = createAppSlice({
  name: "functionalities",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: (create) => ({
    setAttachedFilesOptions: create.reducer((state, action: PayloadAction<AttachedFilesOptions>) => {
      state.attachedFilesOptions = action.payload;
    }),
    refreshAttachedFiles: create.reducer((state) => {
      state.attachedFilesRefresher += 1;
    }),
    setPreviewFile: create.reducer((state, action: PayloadAction<PreviewFile | null>) => {
      state.previewFile = action.payload;
    }),
  }),
  // You can define your selectors here. These selectors receive the slice
  // state as their first argument.
  selectors: {
    selectAttachedFilesOptions: (state) => state.attachedFilesOptions,
    selectAttachedFilesEnabled: (state) => state.attachedFilesOptions.enabled,
    selectAttachedFilesRefresher: (state) => state.attachedFilesRefresher,
    selectPreviewFile: (state) => state.previewFile,
  },
});

// Action creators are generated for each case reducer function.
export const {
  setAttachedFilesOptions,
  refreshAttachedFiles,
  setPreviewFile,
} = functionalitiesSlice.actions;

// Selectors returned by `slice.selectors` take the root state as their first argument.
export const {
  selectAttachedFilesOptions,
  selectAttachedFilesEnabled,
  selectAttachedFilesRefresher,
  selectPreviewFile,
} = functionalitiesSlice.selectors;
