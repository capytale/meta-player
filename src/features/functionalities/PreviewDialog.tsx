import { FC, memo, useEffect, useRef, useState } from "react";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import {
  PreviewFile,
  selectPreviewFile,
  setPreviewFile,
} from "./functionalitiesSlice";
import { Dialog } from "primereact/dialog";

const HighlightedCode = memo(({ code }: { code: string }) => {
  const codeRef = useRef<HTMLDivElement>(null);
  useEffect(() => {
    if (codeRef.current != null) {
      if ((window as any).hljs) {
        (window as any).hljs.highlightBlock(codeRef.current);
      } else {
        console.warn("Highlight.js not loaded");
      }
    }
  }, [code]);

  return (
    <pre>
      <code
        ref={codeRef}
        style={{ padding: 0, background: "white", maxWidth: "100%" }}
      >
        {code}
      </code>
    </pre>
  );
});

const PreviewDialog: FC = () => {
  const previewFile = useAppSelector(selectPreviewFile);
  const [persistentPreviewFile, setPersistentPreviewFile] =
    useState<PreviewFile | null>(null);

  const dispatch = useAppDispatch();
  const visible = previewFile != null;

  useEffect(() => {
    if (previewFile != null) {
      setPersistentPreviewFile(previewFile);
    }
  }, [previewFile]);

  if (persistentPreviewFile == null) {
    return null;
  }

  return (
    <Dialog
      visible={visible}
      header={persistentPreviewFile.title}
      onHide={() => {
        dispatch(setPreviewFile(null));
      }}
      style={{ maxWidth: "100%" }}
    >
      {persistentPreviewFile.type === "text" ? (
        <HighlightedCode code={persistentPreviewFile.content} />
      ) : (
        <img
          src={persistentPreviewFile.url}
          alt={persistentPreviewFile.title}
          style={{
            maxWidth: "100%",
          }}
        />
      )}
    </Dialog>
  );
};

export default PreviewDialog;
