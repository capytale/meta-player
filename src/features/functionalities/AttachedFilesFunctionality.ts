import { FC, useEffect } from "react";
import { AttachedFilesOptions, setAttachedFilesOptions } from "./functionalitiesSlice";
import { useAppDispatch } from "../../app/hooks";

type AttachedFilesFunctionalityProps = Omit<AttachedFilesOptions, "enabled">;

const AttachedFilesFunctionality: FC<AttachedFilesFunctionalityProps> = (props) => {
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(setAttachedFilesOptions({
      ...props,
      enabled: true,
    }));
    return () => {
      dispatch(setAttachedFilesOptions({
        ...props,
        enabled: false,
      }));
    };
  });
  return null;
}

export default AttachedFilesFunctionality;