export type AntiCheatOptions = {
  preserveDom: boolean;
  hasIframes: boolean;
};

export type UIOptions = {
  closePedagoByDefault: boolean;
  noWorkflow: boolean;
  noSaveForStudents: boolean;
};