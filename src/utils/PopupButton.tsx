import { Button, ButtonProps } from "primereact/button";
import { MenuItem } from "primereact/menuitem";
import { TieredMenu } from "primereact/tieredmenu";
import { useMountEffect } from "primereact/hooks";
import {
  IconType,
  ObjectUtils,
  UniqueComponentId,
  classNames,
} from "primereact/utils";
import {
  CSSProperties,
  KeyboardEventHandler,
  MouseEventHandler,
  ReactNode,
  useRef,
  useState,
} from "react";
import { Tooltip } from "primereact/tooltip";
import { TooltipOptions } from "primereact/tooltip/tooltipoptions";

type PopupButtonProps = {
  id?: string;
  icon?: IconType<ButtonProps>;
  buttonClassName?: string;
  disabled?: boolean;
  buttonProps?: ButtonProps;
  size?: "small" | "large";
  severity?: "secondary" | "success" | "info" | "warning" | "danger" | "help";
  outlined?: boolean;
  text?: boolean;
  raised?: boolean;
  unstyled?: boolean;
  onButtonKeydown?: KeyboardEventHandler<HTMLButtonElement>;
  model?: MenuItem[];
  appendTo?: HTMLElement | "self" | (() => HTMLElement) | null;
  tooltip?: ReactNode;
  menuStyle?: CSSProperties;
  autoZIndex?: boolean;
  baseZIndex?: number;
  menuClassName?: string;
  tooltipOptions?: TooltipOptions;
};

const PopupButton: React.FC<PopupButtonProps> = ({
  id,
  icon,
  buttonClassName,
  disabled,
  buttonProps,
  size,
  severity,
  outlined,
  text,
  raised,
  unstyled,
  onButtonKeydown,
  model,
  appendTo,
  tooltip,
  menuStyle,
  autoZIndex,
  baseZIndex,
  menuClassName,
  tooltipOptions,
  ...props
}: PopupButtonProps) => {
  const [idState, setIdState] = useState(id);
  useMountEffect(() => {
    if (!idState) {
      setIdState(UniqueComponentId());
    }
  });

  const [overlayVisibleState, _setOverlayVisibleState] =
    useState<boolean>(false);
  const menuRef = useRef(null);
  const elementRef = useRef(null);

  const hasTooltip = ObjectUtils.isNotEmpty(tooltip);

  const onDropdownButtonClick: MouseEventHandler<HTMLButtonElement> = (
    event,
  ) => {
    menuRef && (menuRef.current as any)?.toggle(event);
  };

  const menuId = idState + "_overlay";

  return (
    <>
      <div {...props}>
        <Button
          type="button"
          className={classNames(buttonClassName)}
          icon={icon}
          onClick={onDropdownButtonClick}
          disabled={disabled}
          aria-expanded={overlayVisibleState}
          aria-haspopup="true"
          aria-controls={menuId}
          {...buttonProps}
          size={size}
          severity={severity}
          outlined={outlined}
          text={text}
          raised={raised}
          onKeyDown={onButtonKeydown}
          unstyled={unstyled}
        />
        <TieredMenu
          ref={menuRef}
          popup={true}
          unstyled={unstyled}
          model={model}
          appendTo={appendTo}
          id={menuId}
          style={menuStyle}
          autoZIndex={autoZIndex}
          baseZIndex={baseZIndex}
          className={menuClassName}
        />
      </div>
      {hasTooltip && (
        <Tooltip
          target={elementRef}
          content={tooltip}
          {...tooltipOptions}
        />
      )}
    </>
  );
};

export default PopupButton;