export function getIdFromUrl() {
  const params = new URLSearchParams(window.location.search);
  const id = params.get("id");
  if (id !== null) {
    return parseInt(id, 10);
  }
  return null;
}
