import { Button, ButtonProps } from "primereact/button";
import React from "react";

type ButtonDoubleIconProps = Exclude<
  ButtonProps,
  "icon" | "children" | "iconPos"
> & {
  leftIcon: string;
  rightIcon: string;
};

const ButtonDoubleIcon: React.FC<ButtonDoubleIconProps> = function ({
  label,
  leftIcon,
  rightIcon,
  ...props
}) {
  return (
    <Button {...props}>
      <span
        className={"p-button-icon p-c p-button-icon-left " + leftIcon}
        data-pc-section="icon"
      ></span>
      <span className="p-button-label p-c" data-pc-section="label">
        {label}
      </span>
      <span
        className={"p-button-icon p-c p-button-icon-right " + rightIcon}
        data-pc-section="icon"
      ></span>
    </Button>
  );
};

export default ButtonDoubleIcon;
