async function getSeverTime() {
  const res = await fetch("/vanilla/time-s.php");
  return parseInt(await res.text()) * 1000;
}

class SeverClock {
  private offset: number;
  private lastSync: number;

  constructor() {
    this.offset = 0;
    this.lastSync = 0;
    this.sync();
  }

  async sync() {
    const offsets = [];
    for (let i = 0; i < 3; i++) {
      const serverTime = await getSeverTime();
      const localTime = Date.now();
      offsets.push(serverTime - localTime);
    }
    this.offset = Math.max(...offsets);
    this.lastSync = Date.now();
  }

  now() {
    return Date.now() + this.offset;
  }

  get lastSyncTime() {
    return this.lastSync;
  }

  get offsetTime() {
    return this.offset;
  }
}

const serverClock = new SeverClock();

export default serverClock;
