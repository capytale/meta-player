export function downloadFile(url: string, name?: string) {
  const a = document.createElement("a");
  a.href = url;
  a.download = name || url.split("/").pop() || "download";
  document.body.appendChild(a);
  a.click();
  document.body.removeChild(a);
}