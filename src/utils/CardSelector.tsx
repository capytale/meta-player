import { RadioButton } from "primereact/radiobutton";
import styles from "./style.module.scss";

type CardSelectorProps<ValueType> = {
  options: { title: string; description: string; value: ValueType }[];
  selected?: ValueType | null;
  onChange: (value: ValueType) => void | Promise<void>;
};
const CardSelector = function <ValueType>(props: CardSelectorProps<ValueType>) {
  return (
    <>
      <div className={styles.cardSelector}>
        {props.options.map((option) => (
          <div
            className={styles.cardSelectorOption}
            data-selected={props.selected === option.value}
            role="button"
            aria-label={`Sélectionner l'option ${option.title}`}
            onClick={() => {
              props.onChange(option.value);
            }}
          >
            <div className={styles.cardSelectorOptionRadio}>
              <RadioButton checked={props.selected === option.value} />
            </div>
            <div className={styles.cardSelectorOptionContent}>
              <div className={styles.cardSelectorOptionTitle}>
                {option.title}
              </div>
              <div className={styles.cardSelectorOptionDescription}>
                {option.description}
              </div>
            </div>
          </div>
        ))}
      </div>
    </>
  );
};

export default CardSelector;
