// From https://github.com/streamich/react-use/blob/master/src/useFullscreen.ts
// Modified to use <body> instead of react ref

import { useState, useEffect, useLayoutEffect } from "react";
import screenfull from "screenfull";

const noop = () => {};
const isBrowser = typeof window !== "undefined";

const useIsomorphicLayoutEffect = isBrowser ? useLayoutEffect : useEffect;

export interface FullScreenOptions {
  onClose?: (error?: Error) => void;
}

const useFullscreen = (
  enabled: boolean,
  onClose: (error?: Error) => void = noop,
): boolean => {
  const [isFullscreen, setIsFullscreen] = useState(enabled);

  useIsomorphicLayoutEffect(() => {
    if (!enabled) {
      return;
    }

    const onChange = () => {
      if (screenfull.isEnabled) {
        const isScreenfullFullscreen = screenfull.isFullscreen;
        setIsFullscreen(isScreenfullFullscreen);
        if (!isScreenfullFullscreen) {
          onClose();
        }
      }
    };

    if (screenfull.isEnabled) {
      try {
        screenfull.request(window.document.body);
        setIsFullscreen(true);
      } catch (error) {
        onClose(error as Error | undefined);
        setIsFullscreen(false);
      }
      screenfull.on("change", onChange);
    } else {
      onClose();
      setIsFullscreen(false);
    }

    return () => {
      setIsFullscreen(false);
      if (screenfull.isEnabled) {
        try {
          screenfull.off("change", onChange);
          screenfull.exit();
        } catch {}
      }
    };
  }, [enabled]);

  return isFullscreen;
};

export default useFullscreen;
