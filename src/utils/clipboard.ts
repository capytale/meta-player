export async function copyToClipboard(
  text: string,
  callback?: (success: boolean, error?: any) => any,
) {
  try {
    await navigator.clipboard.writeText(text);
    callback?.(true);
  } catch (err) {
    callback?.(false, err);
  }
}
