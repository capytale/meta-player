const settings = {
  STATEMENT_MAX_SIZE: 2000000, // 2 MB
  TOOLTIP_SHOW_DELAY: 500,
};

export default settings;
