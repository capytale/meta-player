import type { Action, ThunkAction } from "@reduxjs/toolkit";
import { combineSlices, configureStore } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";
import { themingSlice } from "../features/theming/themingSlice";
import { layoutSlice } from "../features/layout/layoutSlice";
import { activityDataSlice } from "../features/activityData/activityDataSlice";
import { navbarSlice } from "../features/navbar/navbarSlice";
import { saverSlice } from "../features/activityJS/saverSlice";
import { activitySettingsSlice } from "../features/activitySettings/activitySettingsSlice";
import { functionalitiesSlice } from "../features/functionalities/functionalitiesSlice";

// `combineSlices` automatically combines the reducers using
// their `reducerPath`s, therefore we no longer need to call `combineReducers`.
const rootReducer = combineSlices(
  activityDataSlice,
  themingSlice,
  layoutSlice,
  navbarSlice,
  saverSlice,
  activitySettingsSlice,
  functionalitiesSlice,
);
// Infer the `RootState` type from the root reducer
export type RootState = ReturnType<typeof rootReducer>;

// The store setup is wrapped in `makeStore` to allow reuse
// when setting up tests that need the same store config
export const makeStore = (preloadedState?: Partial<RootState>) => {
  const store = configureStore({
    reducer: rootReducer,
    preloadedState,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware({
        serializableCheck: false
      }),
  });
  // configure listeners using the provided defaults
  // optional, but required for `refetchOnFocus`/`refetchOnReconnect` behaviors
  setupListeners(store.dispatch);
  return store;
};

export const store = makeStore();

// Infer the type of `store`
export type AppStore = typeof store;
// Infer the `AppDispatch` type from the store itself
export type AppDispatch = AppStore["dispatch"];
export type AppThunk<ThunkReturnType = void> = ThunkAction<
  ThunkReturnType,
  RootState,
  unknown,
  Action
>;
