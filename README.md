# Meta Player Capytale

## To put in player HTML

- CSS used by the meta-player:
```html
    <link id="theme-link" rel="stylesheet" href="https://cdn.ac-paris.fr/capytale/meta-player/themes/lara-light-blue/theme.css">
```
- If using the attached files preview capability:
```html
    <link rel="stylesheet" href="https://cdn.ac-paris.fr/highlight/styles/default.css">
    <script src="https://cdn.ac-paris.fr/highlight/highlight.min.js"></script>
```